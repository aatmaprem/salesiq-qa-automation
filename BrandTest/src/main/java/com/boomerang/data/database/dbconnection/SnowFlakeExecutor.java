package com.boomerang.data.database.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.boomerang.jdbc.utils.Constants.ConnectionConstants;
import com.boomerang.jdbc.utils.EnvironmentType;
import com.google.common.base.Throwables;
import com.google.inject.Singleton;

@Singleton
public class SnowFlakeExecutor {

	public static class Snowflake {
		public static final String ACCOUNT = "account";
		public static final String USER = "user";
		public static final String PASSWORD = "password";
		public static final String DB = "db";
		public static final String WAREHOUSE = "warehouse";
		public static final String ROLE = "role";
		public static final String HOST_SUFFIX = ".snowflakecomputing.com";
	}

	public ResultSet executeQuery(String query) throws SQLException {
		String host = System.getProperty("host");

		String dbUrl = "jdbc:snowflake://" + host + ".snowflakecomputing.com";

		try {
			Properties props = new Properties();
			props.put(Snowflake.ACCOUNT, host);
			props.put(Snowflake.USER, System.getProperty("username"));
			props.put(Snowflake.PASSWORD, "abc");
			props.put(Snowflake.DB, System.getProperty("db"));
			if (System.getProperty("Env").toLowerCase().equals("beta")
					|| System.getProperty("Env").toLowerCase().equals("qa")) {
				props.put(ConnectionConstants.ENVIRONMENT, EnvironmentType.beta);
			} else {
				props.put(ConnectionConstants.ENVIRONMENT, EnvironmentType.prod);
			}

			props.put(Snowflake.WAREHOUSE, System.getProperty("warehouse"));
			props.put(Snowflake.ROLE, System.getProperty("role"));
			Class.forName("com.boomerang.jdbc.driver.SnowshiftDriver");
			Connection connection = DriverManager.getConnection(dbUrl, props);
			ResultSet rs = null;
			Statement stmt = connection.createStatement();
			rs = stmt.executeQuery(query);
			return rs;
		} catch (ClassNotFoundException e) {
			throw Throwables.propagate(e);
		}
	}
}
