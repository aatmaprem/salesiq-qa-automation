package com.boomerang.data.database.feedquery;

public interface FinalQuery {

	String FINAL_REVENUE = "SELECT sum(ordered_revenue) AS RETAILERREVENUE FROM brands_cubes.client_sales_widget_data WHERE feed_date >= $MIN_TRANSACTION_DATE AND feed_date <= $MAX_TRANSACTION_DATE AND  (  (  clientdetailsid = '2'  )  );";
}
