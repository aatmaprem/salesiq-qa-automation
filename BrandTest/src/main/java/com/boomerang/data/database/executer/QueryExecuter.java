package com.boomerang.data.database.executer;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.LoggerFactory;

import com.boomerang.canvas.automation.framework.GuiceModuleFactory;
import com.boomerang.data.config.ConfigLoader;
import com.boomerang.data.database.dbconnection.SnowFlakeExecutor;
import com.boomerang.data.database.feedquery.FinalQuery;
import com.boomerang.data.database.feedquery.SourceQuery;
import com.boomerang.data.util.GenericUtil;
import com.boomerang.data.util.LaggedConfig;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;

@Singleton
public class QueryExecuter {
	@Inject
	private SnowFlakeExecutor snowFlakeExecutor;
	@Inject
	private GenericUtil generic;

	LaggedConfig laggedConfig;

	private final org.slf4j.Logger log = LoggerFactory.getLogger(QueryExecuter.class);

	@Inject
	public QueryExecuter(String client) throws IOException {
		Injector injector = Guice.createInjector(GuiceModuleFactory.guiceModule());
		injector.injectMembers(this);
		// snowFlakeExecutor = new SnowFlakeExecutor();
		laggedConfig = new LaggedConfig(ConfigLoader.getClientName());

	}

	private String putTransactionDate(String queryToApply) {
		String max_transaction_date = ConfigLoader.getMaxTransactionDate();
		String min_transaction_date = ConfigLoader.getMinTransactionDate();
		if (max_transaction_date == null)
			max_transaction_date = generic.getCurrentDate();

		queryToApply = queryToApply.replaceAll("\\$MAX_TRANSACTION_DATE", "'" + max_transaction_date + "'")
				.replaceAll("\\$MIN_TRANSACTION_DATE", "'" + min_transaction_date + "'");
		return queryToApply;

	}

	public ResultSet getSourceData() {
		return getResultSet(putTransactionDate(SourceQuery.SOURCE_REVENUE));
	}

	public ResultSet getFinalData() {
		return getResultSet(putTransactionDate(FinalQuery.FINAL_REVENUE));
	}

	public String getSourceQuery() {
		return putTransactionDate(SourceQuery.SOURCE_REVENUE);
	}

	public String getFinalQuery() {
		return putTransactionDate(FinalQuery.FINAL_REVENUE);
	}

	public ResultSet getResultSet(String query) {
		ResultSet rs = null;
		try {
			rs = snowFlakeExecutor.executeQuery(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}

	public double getMismatchedPercentage(double total, double mismatched) {
		if (mismatched > 0 && total > 0) {
			return generic.round((mismatched / total) * 100);
		}
		return 0;
	}

	public Map<String, Long> getRevenue(ResultSet resultSet) {
		Map<String, Long> map = new HashMap<>();
		try {
			if (resultSet.next()) {
				map.put("RETAILER REVENUE", (long) resultSet.getDouble("RETAILERREVENUE"));
			}
		} catch (SQLException e) {
			log.error("Exception in Reading Data from ResultSet", e);
		}
		return map;
	}
}
