package com.boomerang.data.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.slf4j.LoggerFactory;

import com.google.inject.Singleton;

@Singleton
public class GenericUtil {
	private final org.slf4j.Logger log = LoggerFactory.getLogger(GenericUtil.class);

	public double round(double value) {
		DecimalFormat df = new DecimalFormat("#.00");
		return Double.parseDouble(df.format(value));
	}

	public String getMinTransactionDate() {
		return System.getProperty("min_transaction_date");
	}

	public String getMaxTransactionDate() {
		return System.getProperty("max_transaction_date");
	}

	public long getDiffDate(String date1, String date2) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return TimeUnit.DAYS.convert(Math.abs(format.parse(date1).getTime() - format.parse(date2).getTime()),
					TimeUnit.MILLISECONDS);
		} catch (ParseException e) {
			log.info("Exception getting difference of date:", e);
		}
		return 0;

	}

	public String getCurrentDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return dateFormat.format(cal.getTime());
	}

}
