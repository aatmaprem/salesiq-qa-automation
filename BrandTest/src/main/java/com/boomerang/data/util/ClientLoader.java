package com.boomerang.data.util;

import java.io.IOException;

import com.google.inject.Inject;
import com.boomerang.data.config.ConfigLoader;

@SuppressWarnings("unused")
public class ClientLoader {

	private static final String PATH = System.getProperty("user.dir") + "/src/test/resources/";
	private ConfigLoader clientConfig;

	private ClientProperties clientProperties;

	private static ClientLoader clientLoader;

	private String clientName;

	private ClientLoader(String clientName) throws IOException {
		this(PATH, clientName);
		this.clientName = clientName;
	}

	private ClientLoader(String path, String clientName) throws IOException {

		clientConfig = new ConfigLoader(path + clientName);

		clientProperties.setRedshiftDb(clientConfig.getProperty("redshift.db"));
		clientProperties.setRedshfitPass(clientConfig.getProperty("redshift.password"));
		clientProperties.setRedshiftUser(clientConfig.getProperty("redshift.username"));
		clientProperties.setRedshiftHost(clientConfig.getProperty("redshift.host"));

		clientProperties.setRdsDb(clientConfig.getProperty("rds.db"));
		clientProperties.setRdsPass(clientConfig.getProperty("rds.password"));
		clientProperties.setRdsUser(clientConfig.getProperty("rds.username"));
		clientProperties.setRdsHost(clientConfig.getProperty("rds.host"));

		clientProperties.setSnowflakeHost(clientConfig.getProperty("snowflake.host"));
		clientProperties.setSnowflakeWarehouse(clientConfig.getProperty("snowflake.warehouse"));
		clientProperties.setSnowflakeDb(clientConfig.getProperty("snowflake.db"));
		clientProperties.setSnowflakeSchema(clientConfig.getProperty("snowflake.schema"));
		clientProperties.setSnowflakeUser(clientConfig.getProperty("snowflake.username"));
		clientProperties.setSnowflakePass(clientConfig.getProperty("snowflake.password"));

		clientProperties.setSftpHost(clientConfig.getProperty("sftp.host"));
		clientProperties.setSftpUsername(clientConfig.getProperty("sftp.username"));
		clientProperties.setSftpPassword(clientConfig.getProperty("sftp.password"));

		clientProperties.setScrapeDate(clientConfig.getProperty("scrapedate.days"));
		clientProperties.setScrapePercent(clientConfig.getProperty("scrapedate.percent"));
		clientProperties.setPpi_check(clientConfig.getProperty("ppi.check"));
		clientProperties.setSkuRel_check(clientConfig.getProperty("skuRel.check"));
		clientProperties.setRowDropPercent(clientConfig.getProperty("row.drop.percent"));
		clientProperties.setPacDropCheck(clientConfig.getProperty("pac.drop.check"));

		clientProperties.setCmTableName(clientConfig.getProperty("cm.table.name"));
		clientProperties.setMatchingTableName(clientConfig.getProperty("matching.table.name"));
		clientProperties.setCmBaseTableName(clientConfig.getProperty("cm.table.name2"));
		clientProperties.setHasVariations(clientConfig.getProperty("has.variations"));

		clientProperties.setBaseSchema(clientConfig.getProperty("schema.base"));
		clientProperties.setMeasurementSchema(clientConfig.getProperty("schema.measurement"));
		clientProperties.setClientName(clientConfig.getProperty("client.name"));

		clientProperties.setEnv(clientConfig.getProperty("env"));
		clientProperties.setMatchingPACCustomTableName(clientConfig.getProperty("matching.pac.custom.name"));
	}

	public static ClientLoader load(String clientName) throws IOException {
		clientLoader = new ClientLoader(clientName + "/config.properties");
		return clientLoader;
	}

	public ClientProperties getClientProperties() {
		return this.clientProperties;
	}
}
