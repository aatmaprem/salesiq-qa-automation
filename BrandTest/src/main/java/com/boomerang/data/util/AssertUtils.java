package com.boomerang.data.util;

import org.testng.asserts.SoftAssert;

public class AssertUtils {

	SoftAssert softAssert;

	public void dataValidation(double revenueMismatchedpercentage, Long sourceRevenue, Long finalRevenue) {
		softAssert.assertTrue(revenueMismatchedpercentage <= 0.1,
				" The differnece between base and ppm is " + revenueMismatchedpercentage + "%");
		softAssert.assertEquals(sourceRevenue, finalRevenue);
	}

	public void assertInitialization() {
		softAssert = new SoftAssert();
	}

	public void assertEquals(String actaul, String expected, String text) {
		softAssert.assertEquals(actaul, expected, text);
	}

	public void assertEqual(int actaul, int expected, String text) {
		softAssert.assertEquals(actaul, expected, text);
	}

	public void assertAll() {
		softAssert.assertAll();
	}

	public void assertfail(String text) {
		softAssert.fail(text + " List is empty");
	}
}
