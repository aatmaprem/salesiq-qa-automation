package com.boomerang.data.util;

public class CustomReportParameters {

	private String field;

	private Object sourceValue;

	private Object finalValue;
	private Object sourceQuery;

	private Object finalQuery;
	private Object misMatchPer;
	private String tableName;

	public CustomReportParameters(String tableName, String field, Object sourceValue, Object sourceQuery,
			Object finalValue, Object finalQuery, Object misMatchPer) {
		super();
		this.field = field;
		this.tableName = tableName;
		this.sourceValue = sourceValue;
		this.finalValue = finalValue;
		this.finalQuery = finalQuery;
		this.sourceQuery = sourceQuery;
		this.misMatchPer = misMatchPer;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Object getSourceValue() {
		return sourceValue;
	}

	public void setSourceValue(Object sourceValue) {
		this.sourceValue = sourceValue;
	}

	public Object getFinalValue() {
		return finalValue;
	}

	public void setFinalValue(Object finalValue) {
		this.finalValue = finalValue;
	}

	public Object getSourceQuery() {
		return sourceQuery;
	}

	public void setSourceQuery(Object sourceQuery) {
		this.sourceQuery = sourceQuery;
	}

	public Object getFinalQuery() {
		return finalQuery;
	}

	public void setFinalQuery(Object finalQuery) {
		this.finalQuery = finalQuery;
	}

	public Object getMisMatchPer() {
		return misMatchPer;
	}

	public void setMisMatchPer(Object misMatchPer) {
		this.misMatchPer = misMatchPer;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

}
