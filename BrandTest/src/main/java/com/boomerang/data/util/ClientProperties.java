package com.boomerang.data.util;

public class ClientProperties {

	private String redshiftHost;
	private String redshiftDb;
	private String redshiftUser;
	private String redshiftPass;

	private String snowflakeHost;
	private String snowflakeWarehouse;
	private String snowflakeDb;
	private String snowflakeSchema;
	private String snowflakeUser;
	private String snowflakePass;

	private String rdsHost;
	private String rdsDb;
	private String rdsUser;
	private String rdsPass;

	private String sftpHost;
	private String sftpUsername;
	private String sftpPassword;

	private String scrapeDate;
	private String scrapePercent;
	private String ppi_check;
	private String skuRel_check;
	private String rowDropPercent;
	private String pacDropCheck;

	private String cmTableName;
	private String cmBaseTableName;
	private String hasVariations;

	private String matchingTableName;
	private String MatchingPACCustomTableName;

	public String getMatchingPACCustomTableName() {
		return MatchingPACCustomTableName;
	}

	public void setMatchingPACCustomTableName(String MatchingPACCustomTableName) {
		this.MatchingPACCustomTableName = MatchingPACCustomTableName;
	}

	private String baseSchema;
	private String measurementSchema;

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	private String clientName;

	private String env;

	public String getSnowflakeHost() {
		return snowflakeHost;
	}

	public void setSnowflakeHost(String snowflakeHost) {
		this.snowflakeHost = snowflakeHost;
	}

	public String getSnowflakeWarehouse() {
		return snowflakeWarehouse;
	}

	public void setSnowflakeWarehouse(String snowflakeWarehouse) {
		this.snowflakeWarehouse = snowflakeWarehouse;
	}

	public String getSnowflakeDb() {
		return snowflakeDb;
	}

	public void setSnowflakeDb(String snowflakeDb) {
		this.snowflakeDb = snowflakeDb;
	}

	public String getSnowflakeSchema() {
		return snowflakeSchema;
	}

	public void setSnowflakeSchema(String snowflakeSchema) {
		this.snowflakeSchema = snowflakeSchema;
	}

	public String getSnowflakeUser() {
		return snowflakeUser;
	}

	public void setSnowflakeUser(String snowflakeUser) {
		this.snowflakeUser = snowflakeUser;
	}

	public String getSnowflakePass() {
		return snowflakePass;
	}

	public void setSnowflakePass(String snowflakePass) {
		this.snowflakePass = snowflakePass;
	}

	public String getRdsHost() {
		return rdsHost;
	}

	public void setRdsHost(String rdsHost) {
		this.rdsHost = rdsHost;
	}

	public String getRdsDb() {
		return rdsDb;
	}

	public void setRdsDb(String rdsDb) {
		this.rdsDb = rdsDb;
	}

	public String getRdsUser() {
		return rdsUser;
	}

	public void setRdsUser(String rdsUser) {
		this.rdsUser = rdsUser;
	}

	public String getRdsPass() {
		return rdsPass;
	}

	public void setRdsPass(String rdsPass) {
		this.rdsPass = rdsPass;
	}

	public void setRedshiftPass(String redshiftPass) {
		this.redshiftPass = redshiftPass;
	}

	public String getSftpHost() {
		return sftpHost;
	}

	public void setSftpHost(String sftpHost) {
		this.sftpHost = sftpHost;
	}

	public String getSftpUsername() {
		return sftpUsername;
	}

	public void setSftpUsername(String sftpUsername) {
		this.sftpUsername = sftpUsername;
	}

	public String getSftpPassword() {
		return sftpPassword;
	}

	public void setSftpPassword(String sftpPassword) {
		this.sftpPassword = sftpPassword;
	}

	public String getRedshiftHost() {
		return redshiftHost;
	}

	public void setRedshiftHost(String redshiftHost) {
		this.redshiftHost = redshiftHost;
	}

	public String getRedshiftDb() {
		return redshiftDb;
	}

	public void setRedshiftDb(String redshfitDb) {
		this.redshiftDb = redshfitDb;
	}

	public String getRedshiftUser() {
		return redshiftUser;
	}

	public void setRedshiftUser(String redshiftUser) {
		this.redshiftUser = redshiftUser;
	}

	public String getRedshiftPass() {
		return redshiftPass;
	}

	public void setRedshfitPass(String redshiftPass) {
		this.redshiftPass = redshiftPass;
	}

	public String getScrapeDate() {
		return scrapeDate;
	}

	public void setScrapeDate(String scrapeDate) {
		this.scrapeDate = scrapeDate;
	}

	public String getScrapePercent() {
		return scrapePercent;
	}

	public void setScrapePercent(String scrapePercent) {
		this.scrapePercent = scrapePercent;
	}

	public String getPpi_check() {
		return ppi_check;
	}

	public void setPpi_check(String ppi_check) {
		this.ppi_check = ppi_check;
	}

	public String getSkuRel_check() {
		return skuRel_check;
	}

	public void setSkuRel_check(String skuRel_check) {
		this.skuRel_check = skuRel_check;
	}

	public String getRowDropPercent() {
		return rowDropPercent;
	}

	public void setRowDropPercent(String rowDropPercent) {
		this.rowDropPercent = rowDropPercent;
	}

	public String getCmTableName() {
		return cmTableName;
	}

	public void setCmTableName(String cmTableName) {
		this.cmTableName = cmTableName;
	}

	public String getCmBaseTableName() {
		return cmBaseTableName;
	}

	public void setCmBaseTableName(String cmBaseTableName) {
		this.cmBaseTableName = cmBaseTableName;
	}

	public String getHasVariations() {
		return hasVariations;
	}

	public void setHasVariations(String hasVariations) {
		this.hasVariations = hasVariations;
	}

	public String getMatchingTableName() {
		return matchingTableName;
	}

	public void setMatchingTableName(String matchingTableName) {
		this.matchingTableName = matchingTableName;
	}

	public String getPacDropCheck() {
		return pacDropCheck;
	}

	public void setPacDropCheck(String pacDropCheck) {
		this.pacDropCheck = pacDropCheck;
	}

	public String getBaseSchema() {
		return baseSchema;
	}

	public void setBaseSchema(String baseSchema) {
		this.baseSchema = baseSchema;
	}

	public String getMeasurementSchema() {
		return measurementSchema;
	}

	public void setMeasurementSchema(String measurementSchema) {
		this.measurementSchema = measurementSchema;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	@Override
	public String toString() {
		return "ClientProperties [redshiftHost=" + redshiftHost + ", redshiftDb=" + redshiftDb + ", redshiftUser="
				+ redshiftUser + ", redshiftPass=" + redshiftPass + ", rdsHost=" + rdsHost + ", rdsDb=" + rdsDb
				+ ", rdsUser=" + rdsUser + ", rdsPass=" + rdsPass + ", sftpHost=" + sftpHost + ", sftpUsername="
				+ sftpUsername + ", sftpPassword=" + sftpPassword + "matchingTableName=" + matchingTableName
				+ ",snowflakeHost=" + snowflakeHost + ", snowflakeWarehouse=" + snowflakeWarehouse + ", snowflakeDb="
				+ snowflakeDb + ", snowflakeSchema=" + snowflakeSchema + ", snowflakeUser=" + snowflakeUser
				+ ", snowflakePass=" + snowflakePass + "]";
	}
}
