package com.boomerang.data.util;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.boomerang.data.config.ConfigLoader;

public class LaggedConfig {
	private static final String PATH = System.getProperty("user.dir");
	private static final Logger logger = LoggerFactory.getLogger(LaggedConfig.class);

	ConfigLoader clientConfig;

	public LaggedConfig(String clientName) {
		try {
			String laggedValuesPath = PATH + clientName + "/laggedValue.properties";
			File file = new File(laggedValuesPath);
			if (!file.exists())
				laggedValuesPath = PATH + "/laggedValue.properties";
			clientConfig = new ConfigLoader(laggedValuesPath);
		} catch (IOException e) {
			logger.error("Exception in getting config ", e);
		}
	}

	public String getLaggedValue(String key) {// default value will be all
		if (clientConfig.getProperty(key) == null)
			return clientConfig.getProperty("all");
		else
			return clientConfig.getProperty(key);
	}

}
