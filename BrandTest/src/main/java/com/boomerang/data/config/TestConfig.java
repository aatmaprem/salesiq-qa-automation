package com.boomerang.data.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import org.testng.Assert;

/**
 * @param properties
 *            : Points to an instance of Properties. Initialized by loading the
 *            TestConfig.properties file
 * @author syed.daud
 */
public class TestConfig {
	public Properties properties;

	/**
	 * Load the TestConfig..properties file and assign the instance to
	 * properties variable
	 * 
	 * @throws IOException
	 */
	public TestConfig(String fileName) throws IOException {
		properties = new Properties();
		InputStream file;
		file = new FileInputStream(fileName);
		properties.load(file);
	}

	public TestConfig(File file) {
		properties = new Properties();
		InputStream is;
		try {
			is = new FileInputStream(file);
			properties.load(is);
		} catch (FileNotFoundException e) {
			Assert.fail("File does not exist");
		} catch (IOException e) {
			Assert.fail("Error loading properties file");
		}
	}

	public String getProperty(String key) {
		if (properties == null) {
			throw new NullPointerException();
		} else {
			return properties.getProperty(key);
		}
	}

	public Set<String> getKeys() {
		if (properties == null) {
			throw new NullPointerException();
		}
		return properties.stringPropertyNames();
	}

	public static void main(String[] args) throws IOException {

	}
}
