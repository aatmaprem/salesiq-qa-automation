package com.boomerang.data.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import org.testng.Assert;

import com.boomerang.data.database.dbconnection.DbTypes;
import com.boomerang.data.util.ClientLoader;
import com.boomerang.data.util.ClientProperties;

/**
 * @param properties
 *            : Points to an instance of Properties. Initialized by loading the
 *            ConfigLoader.properties file
 */
public class ConfigLoader {
	public Properties properties;

	/**
	 * Load the TestConfig..properties file and assign the instance to
	 * properties variable
	 * 
	 * @throws IOException
	 */
	public ConfigLoader(String fileName) throws IOException {
		properties = new Properties();
		InputStream file;
		file = new FileInputStream(fileName);
		properties.load(file);
	}

	public ConfigLoader(File file) {
		properties = new Properties();
		InputStream is;
		try {
			is = new FileInputStream(file);
			properties.load(is);
		} catch (FileNotFoundException e) {
			Assert.fail("File does not exist");
		} catch (IOException e) {
			Assert.fail("Error loading properties file");
		}
	}

	public String getProperty(String key) {
		if (properties == null) {
			throw new NullPointerException();
		} else {
			return properties.getProperty(key);
		}
	}

	public Set<String> getKeys() {
		if (properties == null) {
			throw new NullPointerException();
		}
		return properties.stringPropertyNames();
	}

	public static String getClientName() {
		return System.getProperty("clientpath");
	}

	public static String getMaxTransactionDate() {
		return System.getProperty("max_transaction_date");
	}

	public static String getMinTransactionDate() {
		return System.getProperty("min_transaction_date");
	}

	public static ClientProperties loadClientProperties(DbTypes db) throws IOException {
		String clientName = "lowes_ca";// ConfigLoader.getClientName();//lowes_ca
										// take as default
		String resourcePath = String.format(System.getProperty("user.dir") + "/src/test/resources/%s/config.properties",
				clientName);
		File file = new File(resourcePath);
		if (!file.exists()) {
			Assert.fail(
					"No such client exists. Please contact Shivasish to setup the config. Client Name : ".toUpperCase()
							+ clientName);
		}
		ClientLoader clientL = ClientLoader.load(clientName);
		return clientL.getClientProperties();

	}

}
