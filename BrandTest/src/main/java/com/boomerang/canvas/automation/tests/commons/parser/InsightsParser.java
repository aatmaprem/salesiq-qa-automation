package com.boomerang.canvas.automation.tests.commons.parser;

import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.boomerang.canvas.automation.api.ParsingService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@LocatorProps(locatorFileName = "locators/insights.properties")
public class InsightsParser implements ParsingService {
	@Inject
	private LocatorService locatorService;

	public String isRevenueWidgetPresent() {
		boolean b = locatorService.isElementPresent("no_Data");
		if (b) {
			return "Data Not Present";
		} else {
			return "Data is Present";
		}
	}

	public String isMarginWidgetPresent() {
		boolean b = locatorService.isElementPresent("no_Data");
		if (b) {
			return "Data Not Present";
		} else {
			return "Data is Present";
		}
	}

	public String isReveneueLostWidgetPresent() {
		boolean b = locatorService.isElementPresent("no_Data");
		if (b) {
			return "Data Not Present";
		} else {
			return "Data is Present";
		}
	}

	public String isSearchPerformacedataPresent() {
		boolean b = locatorService.isElementPresent("no_Data");
		if (b) {
			return "Data Not Present";
		} else {
			return "Data is Present";
		}
	}
}
