package com.boomerang.canvas.automation.api;


/**
 * Marker interface to indicate a parsing service that expose data extraction related API.
 */
public interface ParsingService {
}
