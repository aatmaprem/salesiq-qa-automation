package com.boomerang.canvas.automation.framework;

import com.boomerang.canvas.automation.api.ActionService;
import com.boomerang.canvas.automation.api.LocatorService;
import com.boomerang.canvas.automation.api.NavigatorService;
import com.boomerang.canvas.automation.api.ParsingService;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

public class ListenerUtils {

	private static final Logger LOG = LoggerFactory.getLogger(ListenerUtils.class);

	/**
	 * Finds a field in the class that represents an object of class
	 * <code>T</code>.
	 * 
	 * @param testObject
	 *            The test object that is to be introspected.
	 * @param tClass
	 *            The class that represents the type.
	 * @param <T>
	 *            The generic type parameter.
	 * @return The instance value of the type that is present inside the test
	 *         object.
	 * @throws IllegalStateException
	 *             If any error occurs while accessing the field.
	 */
	public static <T> T getValue(Object testObject, Class<T> tClass) {
		if (testObject == null) {
			return null;
		}
		Field[] declaredFields = testObject.getClass().getDeclaredFields();
		for (Field declaredField : declaredFields) {
			if (tClass.isAssignableFrom(declaredField.getType())) {
				declaredField.setAccessible(true);
				try {
					return (T) declaredField.get(testObject);
				} catch (IllegalAccessException e) {
					LOG.info(e.getMessage(), e);
				}
			}
		}
		return null;

	}

	/**
	 * Finds the web driver instance that is used by the test if one can be
	 * found.
	 * 
	 * @param testObject
	 *            The test object.
	 * @return The web driver if one is found.
	 */
	public static WebDriver getWebDriver(Object testObject) {
		ActionService actionService = getValue(testObject, ActionService.class);
		LocatorService locatorService = getValue(actionService, LocatorService.class);
		WebDriver localWebDriver = getValue(locatorService, WebDriver.class);
		localWebDriver = localWebDriver == null ? getValue(actionService, WebDriver.class) : localWebDriver;
		if (localWebDriver != null) {
			return localWebDriver;
		}

		ParsingService parsingService = getValue(testObject, ParsingService.class);
		locatorService = getValue(actionService, LocatorService.class);
		localWebDriver = getValue(locatorService, WebDriver.class);
		localWebDriver = localWebDriver == null ? getValue(parsingService, WebDriver.class) : localWebDriver;
		if (localWebDriver != null) {
			return localWebDriver;
		}

		locatorService = getValue(testObject, LocatorService.class);
		localWebDriver = getValue(locatorService, WebDriver.class);
		if (localWebDriver != null) {
			return localWebDriver;
		}

		NavigatorService navigatorService = getValue(testObject, NavigatorService.class);
		localWebDriver = getValue(navigatorService, WebDriver.class);
		if (localWebDriver != null) {
			return localWebDriver;
		}
		System.out.println("oi[poipoip[i[po");
		return getValue(testObject, WebDriver.class);
	}
}
