package com.boomerang.canvas.automation.framework;


import java.util.ArrayList;
import java.util.List;

public class TestSuiteList {


    private List <TestSuiteInfo> suiteInfoList;

    public TestSuiteList(List<TestSuiteInfo> suiteInfoList) {
        this.suiteInfoList = new ArrayList<>(suiteInfoList);
    }

    public List<TestSuiteInfo> getSuiteInfoList() {
        return suiteInfoList;
    }
}
