package com.boomerang.canvas.automation.api;

import com.google.common.base.Predicate;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.boomerang.canvas.automation.framework.Constants.*;

@Singleton
public class LocatorService {

	private static final Logger LOG = LoggerFactory.getLogger(LocatorService.class);

	private static ThreadLocal<Map<String, PropertiesConfiguration>> locatorConfig = new ThreadLocal<>();
	static {
		locatorConfig.set(new HashMap<>());
	}

	@Inject
	private WebDriver webDriver;

	@Inject
	private RunConfig runConfig;

	public static void addLocatorConfig(String locatorProps, PropertiesConfiguration config) {
		Map<String, PropertiesConfiguration> configList = locatorConfig.get();
		configList.put(locatorProps, config);
	}

	public static void clearLocatorConfig() {
		locatorConfig.get().clear();
	}

	/**
	 * Finds the list of web element given the locator string.
	 * 
	 * @param locator
	 *            The locator string.
	 * @return The web element list for the locator string passed.
	 */
	public List<WebElement> getWebElements(String locator) {
		String locatorType = locator.split("##")[0];
		String locatorValue = locator.split("##")[1];
		String locatorTypeLower = locatorType.toLowerCase();

		switch (locatorTypeLower) {
		case LOCATOR_ID:
			return webDriver.findElements(By.id(locatorValue));

		case LOCATOR_CLASS_NAME:
		case LOCATOR_CLASS:
			return webDriver.findElements(By.className(locatorValue));

		case LOCATOR_NAME:
			return webDriver.findElements(By.name(locatorValue));

		case LOCATOR_TAGNAME:
		case LOCATOR_TAG:
			return webDriver.findElements(By.tagName(locatorValue));

		case LOCATOR_CSS:
			return webDriver.findElements(By.cssSelector(locatorValue));
		case LOCATOR_LINKTEXT:
			return webDriver.findElements(By.linkText(locatorValue));
		case LOCATOR_PARTIAL_LINK:
			return webDriver.findElements(By.partialLinkText(locatorValue));
		case LOCATOR_XPATH:
			return webDriver.findElements(By.xpath(locatorValue));
		default:
			throw new IllegalArgumentException("Unknown locator type '" + locatorValue + '"');
		}
	}

	/**
	 * Finds and waits for the web element to appear on the page for the given
	 * locator address.
	 * 
	 * @param locator
	 *            The locator.
	 * @return The web element when it appears on the page.
	 */
	public WebElement waitForElementInternal(String locator, int maxWaitTimeInSeconds) {
		WebDriverWait wait = new WebDriverWait(webDriver, runConfig.getMaxWaitTimeAfterClick());
		String locatorType = locator.split("##")[0];
		String locatorValue = locator.split("##")[1];
		String locatorTypeLower = locatorType.toLowerCase();
		switch (locatorTypeLower) {
		case LOCATOR_ID:

			return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locatorValue)));

		case LOCATOR_CLASS_NAME:
		case LOCATOR_CLASS:
			return wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locatorValue)));

		case LOCATOR_NAME:
			return wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locatorValue)));

		case LOCATOR_TAGNAME:
		case LOCATOR_TAG:
			return wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(locatorValue)));

		case LOCATOR_CSS:
			return wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorValue)));
		case LOCATOR_LINKTEXT:
			return wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(locatorValue)));
		case LOCATOR_PARTIAL_LINK:
			return wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(locatorValue)));
		case LOCATOR_XPATH:
			return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locatorValue)));
		default:
			throw new IllegalArgumentException("Unknown locator type '" + locatorValue + '"');
		}
	}

	public WebElement internalElement(String locator) {
		String locatorType = locator.split("##")[0];
		String locatorValue = locator.split("##")[1];
		String locatorTypeLower = locatorType.toLowerCase();
		switch (locatorTypeLower) {
		case LOCATOR_ID:

			return webDriver.findElement(By.id(locatorValue));

		case LOCATOR_CLASS_NAME:
		case LOCATOR_CLASS:
			return webDriver.findElement(By.className(locatorValue));

		case LOCATOR_NAME:
			return webDriver.findElement(By.name(locatorValue));

		case LOCATOR_TAGNAME:
		case LOCATOR_TAG:
			return webDriver.findElement(By.tagName(locatorValue));

		case LOCATOR_CSS:
			return webDriver.findElement(By.cssSelector(locatorValue));
		case LOCATOR_LINKTEXT:
			return webDriver.findElement(By.linkText(locatorValue));
		case LOCATOR_PARTIAL_LINK:
			return webDriver.findElement(By.partialLinkText(locatorValue));
		case LOCATOR_XPATH:
			return webDriver.findElement(By.xpath(locatorValue));
		default:
			throw new IllegalArgumentException("Unknown locator type '" + locatorValue + '"');
		}
	}

	/**
	 * Getting the CSS value for jsoup locator address.
	 * 
	 * @param locator
	 *            The locator.
	 * @return CSS value.
	 */
	public String elementlocatorvalue(String locator) {
		String locatorValue = locator.split("##")[1];
		return locatorValue;
	}

	public String jsoupElementLocatorValue(String locatorKey) {
		return elementlocatorvalue(resolveKey(locatorKey));
	}

	public WebElement waitForWebElement(String locatorKey) {
		return waitForWebElement(locatorKey, 0);
	}

	public WebElement waitForWebElement(String locatorKey, int maxWaitTimeInSeconds) {
		return waitForElementInternal(resolveKey(locatorKey), maxWaitTimeInSeconds);
	}

	public void waitForPageLoad() {
		WebDriverWait wait = new WebDriverWait(webDriver, runConfig.getMaxWaitTimeAfterClick());
		wait.until(new Predicate<WebDriver>() {
			@Override
			public boolean apply(WebDriver webDriver) {
				try {
					Thread.sleep(runConfig.getMaxWaitTimeAfterClick() * 500);
					return ((JavascriptExecutor) webDriver).executeScript("return document.readyState")
							.equals("complete");
				} catch (Exception e) {
					LOG.warn(e.getMessage(), e);

					return false;
				}
			}
		});
	}

	public void waitForPageToLoad() {
		WebDriverWait wait = new WebDriverWait(webDriver, runConfig.getMaxWaitTimeAfterClick());
		wait.until(new Predicate<WebDriver>() {
			@Override
			public boolean apply(WebDriver webDriver) {
				try {
					Thread.sleep(runConfig.getMaxWaitTimeAfterClick() * 1000);
					return ((JavascriptExecutor) webDriver).executeScript("return document.readyState")
							.equals("complete");
				} catch (Exception e) {
					LOG.warn(e.getMessage(), e);

					return false;
				}
			}
		});
	}

	/**
	 * Wait for element and to perform click action.
	 * 
	 * @param element
	 *            , which element has to click
	 */
	public boolean retryingFindClick(WebElement li) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 2) {
			try {
				li.click();
				result = true;
				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}

	public void waitForButtonToClick(String element) {
		WebElement webElement = waitForWebElement(element);
		// ((JavascriptExecutor)
		// webDriver).executeScript("arguments[0].scrollIntoView();",
		// webElement);
		boolean b2 = webElement.isEnabled();
		if (b2) {
			retryingFindClick(waitForWebElement(element));
			try {
				Thread.sleep(runConfig.getMaxWaitTimeAfterClick() * 400);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void waitForButtonToClickLink(String element) {
		WebElement webElement = waitForWebElement(element);
		((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView();", webElement);
		boolean b2 = webElement.isEnabled();
		if (b2) {
			try {
				Thread.sleep(runConfig.getMaxWaitTimeAfterClick() * 20);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			waitForWebElement(element).click();
			try {
				Thread.sleep(runConfig.getMaxWaitTimeAfterClick() * 20);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void waitForButtonToNavigate(String element) {
		WebElement webElement = waitForWebElement(element);
		((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView();", webElement);
		boolean b2 = webElement.isEnabled();
		if (b2) {
			waitForWebElement(element).click();
			try {
				Thread.sleep(runConfig.getMaxWaitTimeAfterClick() * 30);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public List<WebElement> findWebElements(String locatorKey) {
		try {
			Thread.sleep(runConfig.getMaxWaitTimeAfterClick() * 100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return getWebElements(resolveKey(locatorKey));
	}

	private String resolveKey(String key) {
		for (Map.Entry<String, PropertiesConfiguration> entry : locatorConfig.get().entrySet()) {
			String resolvedValue = (String) entry.getValue().getProperty(key);
			if (resolvedValue != null) {
				return resolvedValue;

			}
		}
		return null;
	}

	public boolean isElementPresent(String elementlocator) {
		try {
			internalElement(resolveKey(elementlocator));
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public void implicitWiat(int time) {
		webDriver.manage().timeouts().implicitlyWait(time, TimeUnit.MILLISECONDS);
	}
}
