package com.boomerang.canvas.automation.framework;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener2;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.internal.ConstructorOrMethod;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.boomerang.canvas.automation.api.ContextService;
import com.boomerang.canvas.automation.api.DisableScreenshot;
import com.boomerang.canvas.automation.api.RunConfig;
import com.boomerang.canvas.automation.api.TestExecutionContext;
import com.boomerang.data.util.CustomReportUtils;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(CustomReportUtils.class)
public class ScreenshotListener implements IInvokedMethodListener2 {

	private static final Logger LOG = LoggerFactory.getLogger(ScreenshotListener.class);

	// private static String TIMESTAMP = new
	// SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

	@Inject
	private RunConfig runConfig;

	public ExtentReports report;
	public ExtentTest test;

	public ScreenshotListener() {
		Injector injector = Guice.createInjector(GuiceModuleFactory.guiceModule());
		injector.injectMembers(this);
	}

	@Override
	public void beforeInvocation(IInvokedMethod invokedMethod, ITestResult testResult, ITestContext context) {
		ConstructorOrMethod constructorOrMethod = invokedMethod.getTestMethod().getConstructorOrMethod();
		Method method = constructorOrMethod.getMethod();
		Test isTest = method.getAnnotation(Test.class);
		DisableScreenshot disableScreenshot = method.getAnnotation(DisableScreenshot.class);
		if (isTest == null || disableScreenshot != null) {
			return;
		}
		createReport(isTest.testName(), isTest.description());

	}

	@Override
	public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {

		beforeInvocation(method, testResult, testResult.getTestContext());
	}

	@Override
	public void afterInvocation(IInvokedMethod method, ITestResult testResult) {

		afterInvocation(method, testResult, testResult.getTestContext());

	}

	@Override
	public void afterInvocation(IInvokedMethod invokedMethod, ITestResult testResult, ITestContext context) {
		ConstructorOrMethod constructorOrMethod = invokedMethod.getTestMethod().getConstructorOrMethod();
		Method method = constructorOrMethod.getMethod();
		Test isTest = method.getAnnotation(Test.class);
		Test testAnnotation = method.getAnnotation(Test.class);
		DisableScreenshot disableScreenshot = method.getAnnotation(DisableScreenshot.class);
		if (isTest == null || disableScreenshot != null) {
			return;
		}
		TestExecutionContext executionContext = ContextService.getExecutionContext();
		String suiteName = executionContext.getTestSuiteName();
		if (suiteName.toLowerCase().equals("basicsanitytest")) {
			System.out.println("basic sanity");
			try {
				takeScreenshot(testResult, testAnnotation.testName(), testAnnotation.description(),
						ListenerUtils.getWebDriver(testResult.getInstance()));
			} catch (IOException e) {
			}
		} else {
			try {
				takeCanvasScreenshot(testResult, testAnnotation.testName(), testAnnotation.description(),
						ListenerUtils.getWebDriver(testResult.getInstance()));
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Creates the extent report to which the screenshot would be added.
	 * 
	 * @param suiteName
	 *            The name of the test suite.
	 * @param testcase
	 *            The name of the test case.
	 */
	private void createReport(String testcase, String Description) {
		TestExecutionContext executionContext = ContextService.getExecutionContext();
		report = executionContext.getExtentReports();
		test = report.startTest(testcase);
		test.assignCategory(executionContext.getTestSuiteName());
		executionContext.setExtentTest(test);
	}

	/**
	 * Captures and keeps the screenshot to the file system.
	 * 
	 * @param testResult
	 *            The test result.
	 * @param testcase
	 *            The name of the test case.
	 * @throws IOException
	 *             if any error occurs while saving the screenshot to the file
	 *             system.
	 */
	private void takeScreenshot(ITestResult testResult, String testcase, String description, WebDriver webDriver)
			throws IOException {
		TestExecutionContext executionContext = ContextService.getExecutionContext();
		String suiteName = executionContext.getTestSuiteName();
		String screenshotName = suiteName + TestSeriesRunner.TIMESTAMP + "-" + testcase;
		String screenshot = takeScreenshot(screenshotName, executionContext.getTestSuiteName(), webDriver);
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		Reporter.setCurrentTestResult(testResult); //// output gets lost
		// without
		// //// this entry
		// Reporter.log("<a title= \"title\" href=\"" + screenshot + "\">" +
		// "<img width=\"700\" height=\"550\" src=\""
		// + screenshot + "\"></a>");
		// test = executionContext.getExtentTest();
		// int result = testResult.getStatus();
		// if (result == 1) {
		// test.log(LogStatus.PASS, suiteName + TestSeriesRunner.TIMESTAMP + "-"
		// + "Screencast below:"
		// + test.addScreenCapture("./html/" + screenshotName + ".png"),
		// description);
		// } else if (result == 2) {
		// test.log(LogStatus.FAIL,
		// suiteName + TestSeriesRunner.TIMESTAMP + "-" + "Screencast below:"
		// + test.addScreenCapture("./html/" + screenshotName + ".png") +
		// description,
		// testResult.getThrowable());
		// } else if (result == 3) {
		// test.log(LogStatus.SKIP,
		// suiteName + TestSeriesRunner.TIMESTAMP + "-" + "Screencast below:"
		// + test.addScreenCapture("./html/" + screenshotName + ".png") +
		// description,
		// testResult.getThrowable());
		// }
		// report.endTest(test);
		// report.flush();
	}

	private void takeCanvasScreenshot(ITestResult testResult, String testcase, String description, WebDriver webDriver)
			throws IOException {
		TestExecutionContext executionContext = ContextService.getExecutionContext();
		String suiteName = executionContext.getTestSuiteName();
		System.out.println("suiteName" + suiteName);
		String screenshotName = suiteName + TestSeriesRunner.TIMESTAMP + "-" + testcase;
		String screenshot = takeCanvasScreenshot(screenshotName, executionContext.getTestSuiteName(), webDriver);
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		Reporter.setCurrentTestResult(testResult); //// output gets lost without
													//// this entry
		Reporter.log("<a title= \"title\" href=\"" + screenshot + "\">" + "<img width=\"700\" height=\"550\" src=\""
				+ screenshot + "\"></a>");
		test = executionContext.getExtentTest();
		int result = testResult.getStatus();
		if (result == 1) {
			test.log(LogStatus.PASS, suiteName + TestSeriesRunner.TIMESTAMP + "-" + "Screencast below:"
					+ test.addScreenCapture("./html/" + screenshotName + ".png"), description);
		} else if (result == 2) {
			test.log(LogStatus.FAIL,
					suiteName + TestSeriesRunner.TIMESTAMP + "-" + "Screencast below:"
							+ test.addScreenCapture("./html/" + screenshotName + ".png") + description,
					testResult.getThrowable());
		} else if (result == 3) {
			test.log(LogStatus.SKIP,
					suiteName + TestSeriesRunner.TIMESTAMP + "-" + "Screencast below:"
							+ test.addScreenCapture("./html/" + screenshotName + ".png") + description,
					testResult.getThrowable());
		}
		report.endTest(test);
		report.flush();
	}

	private String takeScreenshot(String screenshot, String suiteName, WebDriver webDriver) throws IOException {
		String workspace = runConfig.getUserDir();
		String image;
		TestSeriesRunner series = new TestSeriesRunner();
		String screenLoad = workspace + "/Brands_Data-Validation" + TestSeriesRunner.TIMESTAMP + "/html/";
		File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(screenLoad + screenshot + ".png"));
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		image = screenshot + ".png";
		Reporter.log("<a title= \"title\" href=\"" + image + "\">" + "<img width=\"700\" height=\"550\" src=\"" + image
				+ "\"></a>");
		return image;
	}

	private String takeCanvasScreenshot(String screenshot, String suiteName, WebDriver webDriver) throws IOException {
		String workspace = runConfig.getUserDir();
		String image;
		TestSeriesRunner series = new TestSeriesRunner();
		String screenLoad = workspace + "/" + series.getSeries() + "-Suite" + TestSeriesRunner.TIMESTAMP + "-"
				+ runConfig.getEnv() + "-" + runConfig.getClientName() + "-" + runConfig.getBrowser() + "/html/";
		File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(screenLoad + screenshot + ".png"));
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		image = screenshot + ".png";
		Reporter.log("<a title= \"title\" href=\"" + image + "\">" + "<img width=\"700\" height=\"550\" src=\"" + image
				+ "\"></a>");
		return image;
	}

	public void uploadReport(String workspace) throws IOException, InterruptedException {
		AmazonS3 s3 = new AmazonS3Client();
		String bucketName = "boomerang-qa-report";
		String folderName = "Brands_Data-Validation" + TestSeriesRunner.TIMESTAMP;
		File outputFolder = new File(workspace + "/" + folderName);
		File[] listOfFiles = outputFolder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				createFolder(bucketName, folderName, s3);

				String fileName = folderName + "/" + listOfFiles[i].getName();
				s3.putObject(new PutObjectRequest("boomerang-qa-report", fileName,
						new File(workspace + "/" + folderName + "/" + listOfFiles[i].getName()))
								.withCannedAcl(CannedAccessControlList.PublicReadWrite));
			} else if (listOfFiles[i].isDirectory()) {
				File[] listOfFiles1 = listOfFiles[i].listFiles();
				for (int j = 0; j < listOfFiles1.length; j++) {
					if (listOfFiles1[j].isFile()) {
						String foldername = folderName + "/" + listOfFiles[i].getName();
						createFolder(bucketName, foldername, s3);
						String fileName = folderName + "/" + listOfFiles[i].getName() + "/" + listOfFiles1[j].getName();
						s3.putObject(new PutObjectRequest(bucketName, fileName,
								new File(workspace + "/" + folderName + "/" + listOfFiles[i].getName() + "/"
										+ listOfFiles1[j].getName()))
												.withCannedAcl(CannedAccessControlList.PublicReadWrite));

					} else if (listOfFiles1[j].isDirectory()) {
						File[] listOfFiles2 = listOfFiles1[j].listFiles();
						for (int k = 0; k < listOfFiles2.length; k++) {
							if (listOfFiles2[k].isFile()) {
								String foldername = folderName + "/" + listOfFiles[i].getName() + "/"
										+ listOfFiles1[j].getName();
								createFolder(bucketName, foldername, s3);
								String fileName = folderName + "/" + listOfFiles[i].getName() + "/"
										+ listOfFiles1[j].getName() + "/" + listOfFiles2[k].getName();
								s3.putObject(new PutObjectRequest(bucketName, fileName,
										new File(workspace + "/" + folderName + "/" + listOfFiles[i].getName() + "/"
												+ listOfFiles1[j].getName() + "/" + listOfFiles2[k].getName()))
														.withCannedAcl(CannedAccessControlList.PublicReadWrite));
							} else if (listOfFiles2[k].isDirectory()) {
							}
						}
					}
				}
			}
		}
		// WebDriver driver = new FirefoxDriver();
		// driver.manage().window().maximize();
		// driver.get("http://boomerang-qa-report.s3-website-us-west-2.amazonaws.com/"
		// + series.getSeries() + "-Suite"
		// + TestSeriesRunner.TIMESTAMP + "-" + runConfig.getEnv() + "-" +
		// runConfig.getClientName() + "-"
		// + runConfig.getBrowser() + "/ExtendedReports.html");
		// Document doc = Jsoup.parse(driver.getPageSource());
		// String passCount = doc.select(".t-pass-count.weight-normal").text();
		// String failCount = doc.select(".t-fail-count.weight-normal").text();
		// String skipCount =
		// doc.select(".t-others-count.weight-normal").text();
		// System.out.println("==================================");
		// System.out.println("TotalPassTestCasesCount : " + passCount);
		// System.out.println("TotalFailTestCasesCount : " + failCount);
		// System.out.println("TotalSkipTestCasesCount : " + skipCount);
		// System.out.println("==================================");
		// driver.close();
		// deleteDirectory(folderName);
	}

	public void deleteDirectory(String path) {

		File file = new File(path);
		if (file.isDirectory()) {
			try {
				FileUtils.deleteDirectory(new File(path)); // deletes the whole
															// folder
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			file.delete();
		}

	}

	public void uploadCanvasReport(String workspace) throws IOException, InterruptedException {
		AmazonS3 s3 = new AmazonS3Client();
		String bucketName = "boomerang-qa-report";
		TestSeriesRunner series = new TestSeriesRunner();
		String folderName = series.getSeries() + "-Suite" + TestSeriesRunner.TIMESTAMP + "-" + runConfig.getEnv() + "-"
				+ runConfig.getClientName() + "-" + runConfig.getBrowser();
		System.out.println("http://boomerang-qa-report.s3-website-us-west-2.amazonaws.com/" + series.getSeries()
				+ "-Suite" + TestSeriesRunner.TIMESTAMP + "-" + runConfig.getEnv() + "-" + runConfig.getClientName()
				+ "-" + runConfig.getBrowser() + "/ExtendedReports.html");
		File outputFolder = new File(workspace + "/" + folderName);
		File[] listOfFiles = outputFolder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				createFolder(bucketName, folderName, s3);

				String fileName = folderName + "/" + listOfFiles[i].getName();
				s3.putObject(new PutObjectRequest("boomerang-qa-report", fileName,
						new File(workspace + "/" + folderName + "/" + listOfFiles[i].getName()))
								.withCannedAcl(CannedAccessControlList.PublicRead));
			} else if (listOfFiles[i].isDirectory()) {
				File[] listOfFiles1 = listOfFiles[i].listFiles();
				for (int j = 0; j < listOfFiles1.length; j++) {
					if (listOfFiles1[j].isFile()) {
						String foldername = folderName + "/" + listOfFiles[i].getName();
						createFolder(bucketName, foldername, s3);
						String fileName = folderName + "/" + listOfFiles[i].getName() + "/" + listOfFiles1[j].getName();
						s3.putObject(new PutObjectRequest(bucketName, fileName,
								new File(workspace + "/" + folderName + "/" + listOfFiles[i].getName() + "/"
										+ listOfFiles1[j].getName()))
												.withCannedAcl(CannedAccessControlList.PublicRead));

					} else if (listOfFiles1[j].isDirectory()) {
						File[] listOfFiles2 = listOfFiles1[j].listFiles();
						for (int k = 0; k < listOfFiles2.length; k++) {
							if (listOfFiles2[k].isFile()) {
								String foldername = folderName + "/" + listOfFiles[i].getName() + "/"
										+ listOfFiles1[j].getName();
								createFolder(bucketName, foldername, s3);
								String fileName = folderName + "/" + listOfFiles[i].getName() + "/"
										+ listOfFiles1[j].getName() + "/" + listOfFiles2[k].getName();
								s3.putObject(new PutObjectRequest(bucketName, fileName,
										new File(workspace + "/" + folderName + "/" + listOfFiles[i].getName() + "/"
												+ listOfFiles1[j].getName() + "/" + listOfFiles2[k].getName()))
														.withCannedAcl(CannedAccessControlList.PublicRead));
							} else if (listOfFiles2[k].isDirectory()) {
							}
						}
					}
				}
			}
		}
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://boomerang-qa-report.s3-website-us-west-2.amazonaws.com/" + series.getSeries() + "-Suite"
				+ TestSeriesRunner.TIMESTAMP + "-" + runConfig.getEnv() + "-" + runConfig.getClientName() + "-"
				+ runConfig.getBrowser() + "/ExtendedReports.html");
		Document doc = Jsoup.parse(driver.getPageSource());
		String passCount = doc.select(".t-pass-count.weight-normal").text();
		String failCount = doc.select(".t-fail-count.weight-normal").text();
		String skipCount = doc.select(".t-others-count.weight-normal").text();
		System.out.println("==================================");
		System.out.println("TotalPassTestCasesCount : " + passCount);
		System.out.println("TotalFailTestCasesCount : " + failCount);
		System.out.println("TotalSkipTestCasesCount : " + skipCount);
		System.out.println("==================================");
		driver.close();
		deleteDirectory(folderName);
	}

	private void deleteFolder(String bucketName, String folderName, AmazonS3 client) {
		List<S3ObjectSummary> fileList = client.listObjects(bucketName, folderName).getObjectSummaries();
		for (S3ObjectSummary file : fileList) {
			client.deleteObject(bucketName, file.getKey());
		}
		client.deleteObject(bucketName, folderName);
	}

	private void createFolder(String bucketName, String folderName, AmazonS3 client) {
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName + "/", emptyContent, metadata);
		// send request to S3 to create folder
		client.putObject(putObjectRequest);
	}
}