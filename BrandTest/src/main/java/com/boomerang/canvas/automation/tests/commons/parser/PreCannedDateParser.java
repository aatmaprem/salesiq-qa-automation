package com.boomerang.canvas.automation.tests.commons.parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.boomerang.canvas.automation.api.ParsingService;
import com.google.inject.Inject;

@LocatorProps(locatorFileName = "locators/precanneddate.properties")
public class PreCannedDateParser implements ParsingService {

	@Inject
	private LocatorService locatorService;

	private String dateFormat = "dd MMM yyyy";
	String year;

	public String cannedDateText() {
		String text = locatorService.waitForWebElement("calender_widget").getAttribute("value");
		System.out.println("text" + text);
		String type[] = text.split(":");
		return type[0].toUpperCase().trim();
	}

	public String getStartDate() throws ParseException {
		String text = locatorService.waitForWebElement("calender_widget").getAttribute("value");
		String type[] = text.split(":");

		String date[] = type[1].toUpperCase().trim().split("-");
		System.out.println(date[0]);
		if (date[0].contains(",")) {
			String startDate[] = date[0].split(",");
			String year = startDate[1].trim();
			String monthdate[] = startDate[0].split(" ");
			String monthnumber = getMonthNumber(monthdate[0]);
			return year + "-" + monthnumber + "-" + monthdate[1];
		} else {
			String startDate[] = date[1].trim().split(",");
			String year = startDate[1].trim();
			String monthdate[] = date[0].split(" ");
			String monthnumber = getMonthNumber(monthdate[0]);
			return year + "-" + monthnumber + "-" + monthdate[1];
		}

	}

	public String getEndDate() throws ParseException {
		String text = locatorService.waitForWebElement("calender_widget").getAttribute("value");
		String type[] = text.split(":");

		String date[] = type[1].toUpperCase().trim().split("-");
		System.out.println(date[1]);
		String startDate[] = date[1].trim().split(",");
		String year = startDate[1].trim();
		String monthdate[] = startDate[0].split(" ");
		String monthnumber = getMonthNumber(monthdate[0]);

		return year + "-" + monthnumber + "-" + monthdate[1];
	}

	public int monthConverter(String month) throws ParseException {
		Date date = new SimpleDateFormat("MMMM").parse(month);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		System.out.println(cal.get(Calendar.MONTH));
		return cal.get(Calendar.MONTH);
	}

	public int dayConverter(String day) throws ParseException {
		Date date = new SimpleDateFormat("D").parse(day);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		System.out.println(cal.get(Calendar.DAY_OF_MONTH));
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	public String getDateWithOutCannedText() {
		String text = locatorService.waitForWebElement("calender_widget").getAttribute("value");
		String type[] = text.split(":");

		String date[] = type[1].toUpperCase().trim().split("-");
		System.out.println(date[0]);
		return date[0];
	}

	public String getMonthNumber(String month) {
		switch (month) {
		case "JAN":
			return "1";
		case "FEB":
			return "2";
		case "MAR":
			return "3";
		case "APR":
			return "4";
		case "MAY":
			return "5";
		case "JUN":
			return "6";
		case "JUL":
			return "7";
		case "AUG":
			return "8";
		case "SEP":
			return "9";
		case "OCT":
			return "10";
		case "NOV":
			return "11";
		case "DEC":
			return "12";
		}
		return "0";
	}

	@SuppressWarnings("unused")
	private int getDay(String date) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
		LocalDate dpToDate = LocalDate.parse(date, dtf);
		return dpToDate.getDayOfMonth();
	}

	@SuppressWarnings("unused")
	private long getDateDifferenceInMonths(String date) throws ParseException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
		LocalDate dpCurDate = LocalDate.parse("01 " + this.getCurrentMonthFromDatePicker(), dtf);
		LocalDate dpToDate = LocalDate.parse(date, dtf);
		return YearMonth.from(dpCurDate).until(dpToDate, ChronoUnit.MONTHS);
	}

	private String getCurrentMonthFromDatePicker() throws ParseException {
		String curDate = getStartDate();
		return curDate;
	}
}
