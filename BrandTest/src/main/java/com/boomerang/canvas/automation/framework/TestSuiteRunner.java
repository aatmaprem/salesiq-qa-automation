package com.boomerang.canvas.automation.framework;

import com.boomerang.canvas.automation.api.ContextService;
import com.boomerang.canvas.automation.api.TestExecutionContext;
import com.boomerang.data.util.CustomReportUtils;

import org.testng.TestNG;
import org.testng.annotations.Listeners;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Listeners(CustomReportUtils.class)
public class TestSuiteRunner {

	public void executeTestSuite(String seriesFileName, TestSuiteInfo testSuite)
			throws ClassNotFoundException, IOException {

		XmlSuite suite = new XmlSuite();
		suite.setName(testSuite.getSuiteName());

		List<XmlClass> classes = new ArrayList<XmlClass>();
		classes.add(new XmlClass(testSuite.getTestClass()));

		XmlTest test = new XmlTest(suite);
		test.setName(testSuite.getSuiteName());
		TestExecutionContext testExecutionContext = new TestExecutionContext();
		testExecutionContext.setTestSuiteInfo(testSuite);
		ContextService.setExecutionContext(testExecutionContext);
		ContextService.setParamsMap(testSuite.getParameters());

		test.setXmlClasses(classes);

		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		TestNG testNG = new TestNG();

		testNG.setListenerClasses(Arrays.asList(TestListener.class, ScreenshotListener.class));
		testNG.setXmlSuites(suites);
		testNG.run();

	}
}
