package com.boomerang.canvas.automation.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface TestSuite {

    /**
     * @return Name of the suite.
     */
    String name ();

    /**
     * @return The description of the test suite.
     */
    String description () default "";
}
