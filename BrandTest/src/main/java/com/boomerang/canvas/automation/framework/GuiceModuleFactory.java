package com.boomerang.canvas.automation.framework;


import com.google.inject.Module;
import org.testng.IModuleFactory;
import org.testng.ITestContext;

public class GuiceModuleFactory implements IModuleFactory {

    private static final GuiceModule GUICE_MODULE = new GuiceModule();

    public static GuiceModule guiceModule () {return GUICE_MODULE;}

    @Override
    public Module createModule(ITestContext context, Class<?> testClass) {
        return GUICE_MODULE;
    }
}
