package com.boomerang.canvas.automation.tests.canvasbasicsanity;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.boomerang.canvas.automation.api.LoginAction;
import com.boomerang.canvas.automation.api.NavigatorService;
import com.boomerang.canvas.automation.api.RunConfig;
import com.boomerang.canvas.automation.api.TestSuite;
import com.boomerang.canvas.automation.framework.GuiceModuleFactory;
import com.boomerang.canvas.automation.framework.TestListener;
import com.boomerang.canvas.automation.tests.commons.actions.DataWorkBenchAction;
import com.boomerang.canvas.automation.tests.commons.actions.InsightsAction;
import com.boomerang.canvas.automation.tests.commons.actions.RecommendationsAction;
import com.boomerang.canvas.automation.tests.commons.parser.DataWorkBenchParser;
import com.boomerang.canvas.automation.tests.commons.parser.InsightsPageParser;
import com.boomerang.canvas.automation.tests.commons.parser.RecommendationsParser;
import com.boomerang.data.util.AssertUtils;
import com.google.inject.Inject;

@TestSuite(name = "CanvasBasicSanityTest", description = "This suite has test cases for widgets presenent")
@Guice(moduleFactory = GuiceModuleFactory.class)
public class CanvasBasicSanityTest {
	private static final Logger LOG = LoggerFactory.getLogger(TestListener.class);

	@Inject
	private RunConfig runConfig;

	@Inject
	private NavigatorService navigatorService;
	@Inject
	SoftAssert softAssert;
	@Inject
	private LoginAction loginAction;
	@Inject
	private DataWorkBenchAction dataWorkBenchAction;
	@Inject
	private DataWorkBenchParser dataWorkBenchParser;
	@Inject
	private InsightsPageParser insightsPageParser;
	@Inject
	private InsightsAction insightsAction;
	@Inject
	private AssertUtils assertUtils;

	@Inject
	private RecommendationsParser recommendationsParser;

	@Inject
	private RecommendationsAction recommendationsAction;

	@Test(enabled = true, testName = "LoginAction", description = "Tests the Login of KC")
	public void LoginAction() throws Exception {
		try {
			login();
		} catch (Exception e) {
			LOG.info("Retry Method");

			login();
		}
	}

	public void login() throws Exception {
		navigatorService.openWebURL(runConfig.getClientURL(), LoginAction.USERNAME_TEXTBOX);
		loginAction.login(runConfig.getUserName(), runConfig.getPassword());
		// preCannedDateAction.clickOnCalendarIcon();
		// preCannedDateAction.dateSelection(ConfigLoader.getMinTransactionDate());
		// preCannedDateAction.dateSelection(ConfigLoader.getMaxTransactionDate());
		// loginAction.dataworkbenchPageNavigation();
		// preCannedDateAction.clickOnCalendarIcon();
		// preCannedDateAction.dateSelection(ConfigLoader.getMinTransactionDate());
		// preCannedDateAction.dateSelection(ConfigLoader.getMaxTransactionDate());
	}

	@Test(dependsOnMethods = "LoginAction", enabled = true, testName = "DataWorkBenchPage-All-NoDataValidation", description = "Valistion of the DataWorkBench page ,it should not show No data without filters")
	public void allWidget() {

		assertUtils.assertInitialization();
		loginAction.dataworkbenchPageNavigation();
		dataWorkBenchAction.allNavigation();
		String noDataText = dataWorkBenchParser.getNoDataText();
		assertUtils.assertEquals(noDataText, "Data is Present", "All Section Data Not Present");
		assertUtils.assertAll();
	}

	@Test(dependsOnMethods = "LoginAction", enabled = true, testName = "DataWorkBenchPage-Catalog-NoDataValidation", description = "Valistion of the DataWorkBench page ,it should not show No data without filters")
	public void catalogWidget() {

		assertUtils.assertInitialization();
		loginAction.dataworkbenchPageNavigation();
		dataWorkBenchAction.catalogNavigation();
		String noDataText = dataWorkBenchParser.getNoDataText();
		assertUtils.assertEquals(noDataText, "Data is Present", "Catalog Data Not Present");
		assertUtils.assertAll();
	}

	@Test(dependsOnMethods = "LoginAction", enabled = true, testName = "DataWorkBenchPage-Price&Promotions-NoDataValidation", description = "Valistion of the DataWorkBench page ,it should not show No data without filters")
	public void priceAndPromotionsWidget() {

		assertUtils.assertInitialization();
		loginAction.dataworkbenchPageNavigation();
		dataWorkBenchAction.priceAndPromotionsNavigation();
		String noDataText = dataWorkBenchParser.getNoDataText();
		assertUtils.assertEquals(noDataText, "Data is Present", "Price&Promotions Data Not Present");
		assertUtils.assertAll();
	}

	@Test(dependsOnMethods = "LoginAction", enabled = true, testName = "DataWorkBenchPage-Sales&Margin-NoDataValidation", description = "Valistion of the DataWorkBench page ,it should not show No data without filters")
	public void salesAndMarginWidget() {

		assertUtils.assertInitialization();
		loginAction.dataworkbenchPageNavigation();
		dataWorkBenchAction.salesAndMarginNavigation();
		String noDataText = dataWorkBenchParser.getNoDataText();
		assertUtils.assertEquals(noDataText, "Data is Present", "Sales&Margin Data Not Present");
		assertUtils.assertAll();
	}

	@Test(dependsOnMethods = "LoginAction", enabled = true, testName = "DataWorkBenchPage-Operations-NoDataValidation", description = "Valistion of the DataWorkBench page ,it should not show No data without filters")
	public void operationsWidget() {

		assertUtils.assertInitialization();
		loginAction.dataworkbenchPageNavigation();
		dataWorkBenchAction.operationsNavigation();
		String noDataText = dataWorkBenchParser.getNoDataText();
		assertUtils.assertEquals(noDataText, "Data is Present", "Operations Data Not Present");
		assertUtils.assertAll();
	}

	@Test(dependsOnMethods = "LoginAction", enabled = true, testName = "DataWorkBenchPage-Search&BSR-NoDataValidation", description = "Valistion of the DataWorkBench page ,it should not show No data without filters")
	public void searchAndBSRWidget() {

		assertUtils.assertInitialization();
		loginAction.dataworkbenchPageNavigation();
		dataWorkBenchAction.searchAndBSRNavigation();
		String noDataText = dataWorkBenchParser.getNoDataText();
		assertUtils.assertEquals(noDataText, "Data is Present", "Search&BSR Data Not Present");
		assertUtils.assertAll();
	}

	@Test(dependsOnMethods = "LoginAction", enabled = true, testName = "DataWorkBenchPage-Content-NoDataValidation", description = "Valistion of the DataWorkBench page ,it should not show No data without filters")
	public void contentWidget() {

		assertUtils.assertInitialization();
		loginAction.dataworkbenchPageNavigation();
		dataWorkBenchAction.contentNavigation();
		String noDataText = dataWorkBenchParser.getNoDataText();
		assertUtils.assertEquals(noDataText, "Data is Present", "Content Data Not Present");
		assertUtils.assertAll();
	}

	@Test(dependsOnMethods = "LoginAction", enabled = true, testName = "DataWorkBenchPage-Competition-NoDataValidation", description = "Valistion of the DataWorkBench page ,it should not show No data without filters")
	public void competitionWidget() {

		assertUtils.assertInitialization();
		loginAction.dataworkbenchPageNavigation();
		dataWorkBenchAction.competitionNavigation();
		String noDataText = dataWorkBenchParser.getNoDataText();
		assertUtils.assertEquals(noDataText, "Data is Present", "Competition Data Not Present");
		assertUtils.assertAll();
	}

	@Test(dependsOnMethods = "LoginAction", enabled = true, testName = "InsightsPage-Overview-NoDataValidation", description = "Valistion of the Insights OverView page ,it should not show No data without filters")
	public void overViewWidget() {

		assertUtils.assertInitialization();
		loginAction.insightsPageNavigation();
		insightsAction.overviewWidget();
		String revenueNoDataText = insightsPageParser.getRevenueLostWidgetNoDataText();
		assertUtils.assertEquals(revenueNoDataText, "Data is Present", "Revneue Data Not Present");
		String marginNoDataText = insightsPageParser.getRevenueLostWidgetNoDataText();
		assertUtils.assertEquals(marginNoDataText, "Data is Present", "Margin Widget Data Not Present");
		String revenueLostnoDataText = insightsPageParser.getRevenueLostWidgetNoDataText();
		assertUtils.assertEquals(revenueLostnoDataText, "Data is Present", "RevenueLost Widget Data Not Present");
		assertUtils.assertAll();
	}

	@Test(dependsOnMethods = "LoginAction", enabled = true, testName = "InsightsPage-SearchPerformance-NoDataValidation", description = "Valistion of the Insights Search Performance page ,it should not show No data without filters")
	public void searchPerformaceWidget() {

		assertUtils.assertInitialization();
		loginAction.insightsPageNavigation();
		insightsAction.searchperformence();
		String revenueNoDataText = insightsPageParser.getSearchPerformaceWidgetNoDataText();
		assertUtils.assertEquals(revenueNoDataText, "Data is Present", "SearchPerformance Data Not Present");
		assertUtils.assertAll();
	}

	@Test(dependsOnMethods = "LoginAction", enabled = false, testName = "Recommendations-PriceCompression-DuplicateSkuValidation", description = "Valistion of  Duplicate SKUs in Price compression section ,it should not show duplicate skus")
	public void pricecompressionWidget() {

		assertUtils.assertInitialization();
		loginAction.recommendationPageNavigation();
		int size = recommendationsAction.getAlertslist();
		System.out.println("size : " + size);

		for (int i = 0; i < size; i++) {
			recommendationsAction.clickOnAlertslist(i);
			String alertTypeName = recommendationsParser.getAlertTypeText(i);
			List<String> list1 = recommendationsParser.getListText();
			List<String> list2 = recommendationsParser.getListText();
			System.out.println(list1.size());
			if (list1.size() != 0) {
				for (int l1 = 0; l1 < list1.size(); l1++) {
					int count = 0;
					System.out.println(list1.get(l1));
					for (int l2 = 0; l2 < list1.size(); l2++) {
						System.out.println(list2.get(l2));
						if (list1.get(l1).equals(list2.get(l2))) {
							count = count + 1;
						}

					}
					assertUtils.assertEqual(1, count, alertTypeName + "Section having duplicate skus");
				}
			} else {
				assertUtils.assertfail(alertTypeName);
			}
		}

		assertUtils.assertAll();
	}
}
