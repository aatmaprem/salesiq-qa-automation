package com.boomerang.canvas.automation.api;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@LocatorProps(locatorFileName = "locators/loginpagelocators.properties")
public class LoginAction implements ActionService {

	public static final String USERNAME_TEXTBOX = "txtbox_username";
	public static final String PASSWORD_TEXTBOX = "txtbox_password";
	public static final String LOGIN_BUTTON = "btn_login";
	public static final String LOGOUT_LINK = "calender_widget_link";
	public static final String LOGIN_ERROR_MESSAGE = "txt_login_err_msg";

	@Inject
	private LocatorService locatorService;

	@Inject
	private NavigatorService navigatorService;

	public void login(String username, String password) throws Exception {
		if (locatorService.isElementPresent(USERNAME_TEXTBOX)) {
			locatorService.waitForWebElement(USERNAME_TEXTBOX).sendKeys(username);
		}
		if (locatorService.isElementPresent(PASSWORD_TEXTBOX)) {
			locatorService.waitForWebElement(PASSWORD_TEXTBOX).sendKeys(password);
		}
		if (locatorService.isElementPresent(LOGIN_BUTTON)) {
			locatorService.waitForWebElement(LOGIN_BUTTON).click();
		}
		locatorService.waitForPageLoad();
	}

	public void insightsPageNavigation() {
		locatorService.waitForButtonToClick("insights_link");
	}

	public void recommendationPageNavigation() {
		locatorService.waitForButtonToClick("recommendation_link");
	}

	public void pageNavigation() {
		locatorService.waitForButtonToClick("insights_link");
	}

	public void dataworkbenchPageNavigation() {
		locatorService.waitForButtonToClick("data_work_bench_link");
	}

	public void dataworkbenchPageAllNavigation() {
		locatorService.waitForButtonToClick("data_work_bench_link");
	}

	public void logout() {
		navigatorService.clickToNextInteraction(LOGOUT_LINK, USERNAME_TEXTBOX);
	}
}
