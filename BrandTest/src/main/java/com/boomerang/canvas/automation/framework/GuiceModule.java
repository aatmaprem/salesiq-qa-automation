package com.boomerang.canvas.automation.framework;

import com.boomerang.canvas.automation.api.*;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.boomerang.canvas.automation.framework.Constants.*;
import static com.boomerang.canvas.automation.framework.Constants.CHROME_DRIVER_PATH_WINDOWS;

public class GuiceModule implements Module {

	@Provides
	@Singleton
	public RunConfig runConfig() {
		try {
			String runConfigFile = System.getProperty(RunProperties.RUN_CONFIG_FILE);
			if (runConfigFile == null) {
				runConfigFile = "tests-config.json";
			}
			return FileUtils.loadConfig(runConfigFile, RunConfig.class);
		} catch (IOException e) {
			throw new IllegalStateException(e.getMessage(), e);
		}
	}

	@Provides
	@Singleton
	public WebDriver getBrowser(RunConfig runConfig) throws IOException {
		String userDir = System.getProperty("user.dir");
		String browser = runConfig.getBrowser().toLowerCase();
		String os = runConfig.getOS().toLowerCase();
		WebDriver driver;
		System.out.println(browser);
		if (browser.equals(CHROME) && os.contains(MAC)) {
			System.setProperty(CHROME_DRIVER_PROPS, userDir + CHROME_DRIVER_PATH_MAC);
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("--kiosk");
			driver = new ChromeDriver(chromeOptions);
		} else if (browser.equals(CHROME) && os.toLowerCase().contains(WINDOWS)) {
			System.setProperty(CHROME_DRIVER_PROPS, userDir + CHROME_DRIVER_PATH_WINDOWS);
			ChromeOptions o = new ChromeOptions();
			o.addArguments("disable-extensions");
			o.addArguments("--start-maximized");
			driver = new ChromeDriver(o);
		} else {
			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("capability.policy.default.Window.Quer‌​yInterface", "allAccess");
			profile.setPreference("capability.policy.default.Window.fram‌​eElement.get", "allAc‌​cess");
			profile.setAcceptUntrustedCertificates(true);
			profile.setAssumeUntrustedCertificateIssuer(true);
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			driver = new FirefoxDriver(capabilities);
			// driver = new FirefoxDriver();
			driver.manage().window().maximize();
		}
		driver.manage().timeouts().implicitlyWait(runConfig.getMaxWaitTimeAfterClick(), TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(runConfig.getMaxWaitTimeAfterClick(), TimeUnit.SECONDS);
		return driver;
	}

	@Override
	public void configure(Binder binder) {
	}
}
