package com.boomerang.canvas.automation.framework;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.internal.collections.Pair;

import com.boomerang.canvas.automation.api.ActionService;
import com.boomerang.canvas.automation.api.ContextService;
import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.boomerang.canvas.automation.api.ParsingService;
import com.boomerang.canvas.automation.api.TestExecutionContext;
import com.boomerang.data.util.CustomReportUtils;
import com.google.inject.Guice;
import com.google.inject.Injector;

@Listeners(CustomReportUtils.class)
public class TestListener implements ITestListener {

	private static final Logger LOG = LoggerFactory.getLogger(TestListener.class);
	private WebDriver webDriver;

	public TestListener() {
		Injector injector = Guice.createInjector(GuiceModuleFactory.guiceModule());
		injector.injectMembers(this);
	}

	@Override
	public void onTestStart(ITestResult result) {
		TestExecutionContext executionContext = ContextService.getExecutionContext();
		Method method = result.getMethod().getConstructorOrMethod().getMethod();
		TestSeriesRunner extentReport = new TestSeriesRunner();
		executionContext.setExtentReports(extentReport.getReport());
		Test testAnnotation = method.getAnnotation(Test.class);
		executionContext.setTestMethodName(testAnnotation.testName());
		executionContext.setTestDescription(testAnnotation.description());
		webDriver = ListenerUtils.getWebDriver(result.getInstance());
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		TestExecutionContext executionContext = ContextService.getExecutionContext();
	}

	@Override
	public void onTestFailure(ITestResult result) {
		TestExecutionContext executionContext = ContextService.getExecutionContext();

	}

	@Override
	public void onTestSkipped(ITestResult result) {
		TestExecutionContext executionContext = ContextService.getExecutionContext();
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

	@Override
	public void onStart(ITestContext context) {
		TestExecutionContext executionContext = ContextService.getExecutionContext();
		Map<String, PropertiesConfiguration> locatorPropsMap = new HashMap<>();
		collectRequiredLocatorsProps(executionContext.getTestSuiteInfo().getTestClass(), locatorPropsMap,
				new HashSet<>());
		for (Map.Entry<String, PropertiesConfiguration> entry : locatorPropsMap.entrySet()) {
			LocatorService.addLocatorConfig(entry.getKey(), entry.getValue());
		}
	}

	/**
	 * Collects the required properties files recursively based on searching
	 * annotation over all classes reachable from test class.
	 * 
	 * @param classToSearch
	 *            The test class.
	 * @param locatorPropsMap
	 *            The map of config name to properties configuration that is
	 *            collected.
	 */
	private void collectRequiredLocatorsProps(Class classToSearch, Map<String, PropertiesConfiguration> locatorPropsMap,
			Set<Class> visitedClassSet) {
		Pair<LocatorProps, PropertiesConfiguration> classLocatorProps = loadLocatorProps(classToSearch);
		if (classLocatorProps != null && locatorPropsMap.get(classLocatorProps.first().locatorFileName()) == null) {
			locatorPropsMap.put(classLocatorProps.first().locatorFileName(), classLocatorProps.second());
		}
		visitedClassSet.add(classToSearch);

		Field[] allFields = classToSearch.getDeclaredFields();
		for (Field field : allFields) {
			Class fieldClass = field.getType();
			boolean isActionClass = ActionService.class.isAssignableFrom(fieldClass);
			boolean isParsingClass = ParsingService.class.isAssignableFrom(fieldClass);
			if ((isActionClass || isParsingClass) && !visitedClassSet.contains(fieldClass)) {
				collectRequiredLocatorsProps(fieldClass, locatorPropsMap, visitedClassSet);
			}
		}
	}

	/**
	 * Loads the locators props location from class's annotation and returns the
	 * <code>PropertiesConfiguration</code>
	 * 
	 * @param locatorPropsClass
	 *            The class where the annotation should be present with
	 *            properties path.
	 * @return The properties loaded from the annotation if onw was present.
	 *         Otherwise, null is returned.
	 */
	private Pair<LocatorProps, PropertiesConfiguration> loadLocatorProps(Class locatorPropsClass) {
		LocatorProps locatorProps = (LocatorProps) locatorPropsClass.getAnnotation(LocatorProps.class);
		try {
			if (locatorProps != null) {

				PropertiesConfiguration config = PropertiesLoader.loadProperties(locatorProps.locatorFileName());
				return Pair.create(locatorProps, config);
			}
		} catch (IOException e) {
			throw new IllegalStateException(e.getMessage(), e);
		}
		return null;
	}

	@Override
	public void onFinish(ITestContext context) {
		String testSuiteName = ContextService.getExecutionContext().getTestSuiteName();
		ContextService.clearExecutionContext();
		ContextService.clearParamsMap();
		LocatorService.clearLocatorConfig();
		closeAllWindows(testSuiteName);

	}

	private void closeAllWindows(String suiteName) {
		if (webDriver == null) {
			return;
		}
		String homeWindow = webDriver.getWindowHandle();
		Set<String> allWindows = webDriver.getWindowHandles();

		// Use Iterator to iterate over windows

		// Verify next window is available
		for (String childWindow : allWindows) {
			// Store the Recruiter window id
			if (!homeWindow.equals(childWindow)) {
				try {
					webDriver.switchTo().window(childWindow);
					webDriver.quit();
				} catch (Exception ignore) {
				}
			}
		}

		try {
			webDriver.switchTo().window(homeWindow);
			webDriver.quit();
		} catch (Exception ignore) {
		}
	}
}