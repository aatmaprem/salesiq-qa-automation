package com.boomerang.canvas.automation.api;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;

@Singleton
public class NavigatorService {

	@Inject
	private LocatorService locatorService;

	@Inject
	private WebDriver webDriver;

	Logger log;

	public void openWebURL(String url, String newURLIdentifier) {
		webDriver.navigate().to(url);
		if (locatorService.isElementPresent(newURLIdentifier)) {
			locatorService.waitForWebElement(newURLIdentifier);
		}
	}

	/**
	 * Gives the formed URL to navigate to for PPM.
	 * 
	 * @param entityType
	 *            The type of the entity.
	 * @param name
	 *            The name of the value passed for the entity (like sku name)
	 * @param value
	 *            The value passed for the entity (like sku id)
	 * @param minDate
	 *            The start date.
	 * @param maxDate
	 *            The end date.
	 * @return Formed URL.
	 */
	// //public String formURL(EntityType entityType, String id, String value,
	// Date minDate, Date maxDate) {
	// return runConfig.getClientURL() + "/" + entityType.formURLSuffix(minDate,
	// maxDate, id, value);
	//
	// }

	/**
	 * Clicks the link to go the next page and waits for the page to load.
	 * 
	 * @param clickText
	 *            The identifier to identify the click with.
	 * @param newPageIdentifier
	 *            The identifier to identify within the next page.
	 */
	public boolean clickToNextInteraction(String clickText, String newPageIdentifier) {
		WebElement webElement = locatorService.waitForWebElement(clickText);
		webElement.click();
		if (newPageIdentifier != null) {
			try {
				locatorService.waitForWebElement(newPageIdentifier);
				return true;
			} catch (RuntimeException e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Clicks the link to go the next page and waits for the page to load.
	 * 
	 * @param clickText
	 *            The identifier to identify the click with.
	 * @param newPageIdentifier
	 *            The identifier to identify within the next page.
	 * @param failureIdentifier
	 *            The identifier to identify the loading of failure page.
	 * @param checkFailureFirst
	 *            True, iff first failure has to be checked.
	 * @return True, if the success condition was met. False, if the failure
	 *         condition was met.
	 */
	public boolean clickToNextInteraction(String clickText, String newPageIdentifier, String failureIdentifier,
			boolean checkFailureFirst) {
		String firstText = checkFailureFirst ? failureIdentifier : newPageIdentifier;
		String secondText = checkFailureFirst ? newPageIdentifier : failureIdentifier;
		WebElement webElement = locatorService.waitForWebElement(clickText);
		webElement.click();
		try {
			locatorService.waitForWebElement(firstText);
			return !checkFailureFirst;
		} catch (RuntimeException e) {
			try {
				locatorService.waitForWebElement(secondText);
				return checkFailureFirst;
			} catch (RuntimeException e1) {
				throw e1;
			}
		}
	}

	/**
	 * Checks if the link has gone stale.
	 * 
	 * @param element
	 *            The link to test.
	 * @param maxWaitTimeInSeconds
	 *            Max time to wait in seconds.
	 * @return True, iff the link has gone stale and no longer available.
	 * @throws InterruptedException
	 *             If an interruption is done to the thread while waiting.
	 */
	@SuppressWarnings("unused")
	private boolean hasLinkGoneStale(WebElement element, int maxWaitTimeInSeconds) throws InterruptedException {
		long startTime = System.currentTimeMillis();
		long currentTime = System.currentTimeMillis();
		while ((currentTime - startTime) / 1000.0 < maxWaitTimeInSeconds) {
			try {
				element.findElement(By.id("does-not-matter"));
				// Thread.sleep(1000);
			} catch (StaleElementReferenceException e) {
				return true;
			}
			currentTime = System.currentTimeMillis();
		}
		return false;
	}
}
