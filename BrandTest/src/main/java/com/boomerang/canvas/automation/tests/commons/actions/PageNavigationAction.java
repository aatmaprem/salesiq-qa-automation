package com.boomerang.canvas.automation.tests.commons.actions;

import com.boomerang.canvas.automation.api.ActionService;
import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@LocatorProps(locatorFileName = "locators/loginpagelocators.properties")
public class PageNavigationAction implements ActionService {

	@Inject
	private LocatorService locatorService;

	public void pageNavigation(String page) {
		locatorService.waitForWebElement(page).click();
		;
	}

}
