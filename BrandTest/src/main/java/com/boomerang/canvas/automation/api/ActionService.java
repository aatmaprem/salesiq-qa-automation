package com.boomerang.canvas.automation.api;

/**
 * Marker interface to mark any service that provides actions (like clicks) on UI.
 */
public interface ActionService {
}
