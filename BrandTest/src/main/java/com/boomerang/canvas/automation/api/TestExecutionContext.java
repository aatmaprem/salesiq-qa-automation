package com.boomerang.canvas.automation.api;


import com.boomerang.canvas.automation.framework.TestSuiteInfo;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class TestExecutionContext {

    private TestSuiteInfo testSuiteInfo;

    private String testMethodName;

    private String testDescription;

    private ExtentReports extentReports;

    private ExtentTest extentTest;


    public TestSuiteInfo getTestSuiteInfo() {
        return testSuiteInfo;
    }

    public void setTestSuiteInfo(TestSuiteInfo testSuiteInfo) {
        this.testSuiteInfo = testSuiteInfo;
    }

    public String getTestSuiteName() {
        return testSuiteInfo.getSuiteName();
    }

    public String getTestSuiteDescription() {
        return testSuiteInfo.getSuiteDescription();
    }


    public String getTestMethodName() {
        return testMethodName;
    }

    public void setTestMethodName(String testMethodName) {
        this.testMethodName = testMethodName;
    }

    public String getTestDescription() {
        return testDescription;
    }

    public void setTestDescription(String testDescription) {
        this.testDescription = testDescription;
    }

    public ExtentReports getExtentReports() {
        return extentReports;
    }

    public void setExtentReports(ExtentReports extentReports) {
        this.extentReports = extentReports;
    }

    public ExtentTest getExtentTest() {
        return extentTest;
    }

    public void setExtentTest(ExtentTest extentTest) {
        this.extentTest = extentTest;
    }
}
