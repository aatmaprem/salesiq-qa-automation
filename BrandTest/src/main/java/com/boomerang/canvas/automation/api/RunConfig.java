package com.boomerang.canvas.automation.api;

import java.net.MalformedURLException;
import java.net.URL;

public class RunConfig {

	private String browser;
	private String clientURL;
	private String userName;
	private String password;
	private int maxWaitTimeAfterClick;

	public RunConfig() {
		if (System.getProperty("Browser") == null) {
			browser = "chrome";
		} else {
			browser = System.getProperty("Browser");
		}
		String[] browserType = browser.split(",");
		for (int i = 0; i < browserType.length; i++) {
			System.setProperty("BrowserType", browserType[i]);
		}
	}

	public String getBrowser() {
		setBrowser();
		return browser;
	}

	public void setBrowser() {
		if (System.getProperty("BrowserType") == null) {
			this.browser = browser;
		} else {
			this.browser = System.getProperty("BrowserType");
		}
	}

	public String getClientURL() {
		setClientURL();
		return clientURL;
	}

	public void setClientURL() {
		if (System.getProperty("ClientURL") == null) {
			this.clientURL = clientURL;
		} else {
			this.clientURL = System.getProperty("ClientURL");
		}
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEnv() {
		if (getClientURL() == null) {
			return null;
		}
		try {
			String host = new URL(getClientURL()).getHost();
			String clientName = host.split("\\.")[0];
			if (clientName.contains("-")) {
				return clientName.split("-")[0];
			} else {
				return "prod";
			}
		} catch (MalformedURLException e) {
			return null;
		}
	}

	public String getClientName() {
		if (getClientURL() == null) {
			return null;
		}
		try {
			String host = new URL(getClientURL()).getHost();
			String clientName = host.split("\\.")[0];
			if (clientName.contains("-")) {
				return clientName.split("-")[1];
			} else {
				return clientName;
			}
		} catch (MalformedURLException e) {
			return null;
		}
	}

	public String getOS() {
		return System.getProperty("os.name");
	}

	public int getMaxWaitTimeAfterClick() {
		if (maxWaitTimeAfterClick <= 0) {
			return 10;
		}

		return maxWaitTimeAfterClick;
	}

	public String getUserDir() {
		return System.getProperty("user.dir");
	}
}
