package com.boomerang.canvas.automation.tests.commons.actions;

import java.util.List;

import com.boomerang.canvas.automation.api.ActionService;
import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.boomerang.canvas.automation.tests.commons.parser.RecommendationsParser;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@LocatorProps(locatorFileName = "locators/recommendations.properties")
public class RecommendationsAction implements ActionService {

	@Inject
	private LocatorService locatorService;
	@Inject
	private RecommendationsParser recommendationsParser;

	public int getAlertslist() {
		return locatorService.findWebElements("alerts_types_list").size();

	}

	public void clickOnAlertslist(int i) {
		locatorService.findWebElements("alerts_types_list").get(i).click();

	}

}
