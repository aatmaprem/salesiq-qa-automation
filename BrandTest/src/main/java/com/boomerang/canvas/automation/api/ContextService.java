package com.boomerang.canvas.automation.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.google.inject.Singleton;

@Singleton
public class ContextService {

    private static ThreadLocal<Map<String, String>> localParamsMap = new ThreadLocal<>();
    private static ThreadLocal<TestExecutionContext> localExecutionContext = new ThreadLocal<>();

    public static void setParamsMap(Map<String, String> paramsMap) {
	localParamsMap.set(paramsMap);
    }

    public static void clearParamsMap() {
	localParamsMap.remove();
    }

    public static void setExecutionContext(TestExecutionContext executionContext) {
	localExecutionContext.set(executionContext);
    }

    public static void clearExecutionContext() {
	localExecutionContext.remove();
    }

    public static TestExecutionContext getExecutionContext() {
	return localExecutionContext.get();
    }

    /**
     * @param paramName
     *            The name of the parameter.
     * @return The value of the parameter.
     */
    public String getParamValue(String paramName) {
	return localParamsMap.get().get(paramName);
    }

    public Date paramValueAsDate(String paramName) {
	String paramValue = getParamValue(paramName);
	if (paramValue == null) {
	    return null;
	}

	SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	try {
	    return df.parse(paramValue);
	} catch (ParseException e) {
	    throw new IllegalArgumentException(e.getMessage(), e);
	}
    }

    public int paramValueAsInt(String paramName) {
	String paramValue = getParamValue(paramName);
	if (paramValue == null) {
	    return 0;
	}
	return Integer.parseInt(paramValue);
    }

    public double paramValueAsDouble(String paramName) {
	String paramValue = getParamValue(paramName);
	if (paramValue == null) {
	    return 0;
	}
	return Double.parseDouble(paramValue);
    }

    public boolean paramValueAsBoolean(String paramName) {
	String paramValue = getParamValue(paramName);
	if (paramValue == null) {
	    return false;
	}
	return Boolean.parseBoolean(paramValue);
    }

}