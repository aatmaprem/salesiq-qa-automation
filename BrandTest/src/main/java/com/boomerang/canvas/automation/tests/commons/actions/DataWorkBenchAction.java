package com.boomerang.canvas.automation.tests.commons.actions;

import com.boomerang.canvas.automation.api.ActionService;
import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@LocatorProps(locatorFileName = "locators/dataworkbench.properties")
public class DataWorkBenchAction implements ActionService {

	@Inject
	private LocatorService locatorService;

	public void allNavigation() {
		locatorService.waitForButtonToClick("all_widget_link");
	}

	public void catalogNavigation() {
		locatorService.waitForButtonToClick("catalog_widget_link");
	}

	public void priceAndPromotionsNavigation() {
		locatorService.waitForButtonToClick("price_promotions_widget_link");
	}

	public void salesAndMarginNavigation() {
		locatorService.waitForButtonToClick("sales_margin_widget_link");
	}

	public void operationsNavigation() {
		locatorService.waitForButtonToClick("operations_widget_link");
	}

	public void searchAndBSRNavigation() {
		locatorService.waitForButtonToClick("search_BSR_widget_link");
	}

	public void contentNavigation() {
		locatorService.waitForButtonToClick("content_widget_link");
	}

	public void competitionNavigation() {
		locatorService.waitForButtonToClick("competition_widget_link");
	}

}
