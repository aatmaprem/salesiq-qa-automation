package com.boomerang.canvas.automation.tests.commons.actions;

import com.boomerang.canvas.automation.api.ActionService;
import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.boomerang.canvas.automation.framework.PickDate;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@LocatorProps(locatorFileName = "locators/precanneddate.properties")
public class PreCannedDateAction implements ActionService {

	@Inject
	private LocatorService locatorService;

	@Inject
	private PickDate pickDate;

	public void clickOnCalendarIcon() {
		locatorService.waitForButtonToClick("calender_widget_link");
	}

	public void dateSelection(String Date) throws InterruptedException {
		pickDate.pickExpDate(Date);
	}
}
