package com.boomerang.canvas.automation.tests.commons.parser;

import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.boomerang.canvas.automation.api.ParserService;
import com.boomerang.canvas.automation.api.ParsingService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.List;

@Singleton
@LocatorProps(locatorFileName = "locators/recommendations.properties")
public class RecommendationsParser implements ParsingService {
	@Inject
	private LocatorService locatorService;
	@Inject
	private ParserService parserService;

	public List<String> getListText() {
		List<String> list = new ArrayList<>();
		int size = locatorService.findWebElements("sku_list").size();
		for (int i = 0; i < size; i++) {
			list.add(locatorService.findWebElements("sku_list").get(i).getText());
		}
		return list;
	}

	public String getAlertTypeText(int i) {
		return locatorService.findWebElements("alerts_types_list").get(i).getText();
	}
}
