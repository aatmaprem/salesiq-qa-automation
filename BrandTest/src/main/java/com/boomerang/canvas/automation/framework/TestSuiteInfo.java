package com.boomerang.canvas.automation.framework;


import com.boomerang.canvas.automation.api.TestSuite;

import java.util.Map;

public class TestSuiteInfo {

    private Class testClass;
    private Map <String, String> parameters;

    public TestSuiteInfo(Class testClass, Map<String, String> parameters) {
        this.testClass = testClass;
        this.parameters = parameters;
    }

    public String getSuiteName() {
        TestSuite testSuite = (TestSuite) testClass.getAnnotation(TestSuite.class);
        if (testSuite != null) {
            return testSuite.name();
        }
        return testClass.getName();
    }

    public String getSuiteDescription () {
        TestSuite testSuite = (TestSuite) testClass.getAnnotation(TestSuite.class);
        if (testSuite != null) {
            return testSuite.description();
        }
        return "";
    }

    public Class getTestClass() {
        return testClass;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }
}
