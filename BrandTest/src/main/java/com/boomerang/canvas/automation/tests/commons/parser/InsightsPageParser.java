package com.boomerang.canvas.automation.tests.commons.parser;

import java.text.ParseException;
import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.boomerang.canvas.automation.api.ParserService;
import com.boomerang.canvas.automation.api.ParsingService;
import com.google.inject.Inject;

@LocatorProps(locatorFileName = "locators/insights.properties")
public class InsightsPageParser implements ParsingService {
	@Inject
	private ParserService parserService;
	@Inject
	private LocatorService locatorService;

	public String getRetailerRevenue() {
		return parserService.parseContent("retailer_revenue");
	}

	public String getRevenueWidgetNoDataText() {
		boolean b = locatorService.isElementPresent("revenue_widget_presence");
		if (b) {
			return "Data Not Present";
		} else {
			return "Data is Present";
		}
	}

	public String getMarginWidgetNoDataText() {
		boolean b = locatorService.isElementPresent("revenue_widget_presence");
		if (b) {
			return "Data Not Present";
		} else {
			return "Data is Present";
		}
	}

	public String getRevenueLostWidgetNoDataText() {
		boolean b = locatorService.isElementPresent("revenue_widget_presence");
		if (b) {
			return "Data Not Present";
		} else {
			return "Data is Present";
		}
	}

	public String getSearchPerformaceWidgetNoDataText() {
		boolean b = locatorService.isElementPresent("revenue_widget_presence");
		if (b) {
			return "Data Not Present";
		} else {
			return "Data is Present";
		}
	}
}
