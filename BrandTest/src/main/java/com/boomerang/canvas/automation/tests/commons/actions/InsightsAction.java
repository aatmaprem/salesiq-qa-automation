package com.boomerang.canvas.automation.tests.commons.actions;

import com.boomerang.canvas.automation.api.ActionService;
import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@LocatorProps(locatorFileName = "locators/insights.properties")
public class InsightsAction implements ActionService {

	@Inject
	private LocatorService locatorService;

	public void overviewWidget() {
		locatorService.waitForButtonToClick("overview_widget_link");
	}

	public void searchperformence() {
		locatorService.waitForButtonToClick("search_performace_widget_link");
	}

	public void priceWar() {
		locatorService.waitForButtonToClick("price_war_widget_link");
	}

}
