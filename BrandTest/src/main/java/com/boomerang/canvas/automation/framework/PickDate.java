package com.boomerang.canvas.automation.framework;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@LocatorProps(locatorFileName = "locators/precanneddate.properties")
public class PickDate {
	@Inject
	private LocatorService locatorService;
	WebElement datePicker;
	List<WebElement> datePicker1;
	List<WebElement> noOfColumns;
	List<String> monthList = Arrays.asList("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV",
			"DEC");
	// Expected Date, Month and Year
	int expMonth;
	int expYear;
	String expDate = null;
	// Calendar Month and Year
	String leftcalMonth = null;
	String leftcalYear = null;
	String rightcalMonth = null;
	String rightcalYear = null;
	boolean dateNotFound;

	public void pickExpDate(String Date) throws InterruptedException {
		dateNotFound = true;

		String[] all = Date.split("-");
		expDate = all[2];
		expMonth = Integer.parseInt(all[1]);
		expYear = Integer.parseInt(all[0]);

		while (dateNotFound) {
			String leftmonthdate = locatorService.waitForWebElement("leftmonthDate").getText();
			String[] date = leftmonthdate.split(" ");
			leftcalMonth = date[0].toUpperCase();
			leftcalYear = date[1];
			String rightmonthdate = locatorService.waitForWebElement("rightmonthDate").getText();
			String[] rightdate = rightmonthdate.split(" ");
			rightcalMonth = rightdate[0].toUpperCase();
			rightcalYear = rightdate[1];
			System.out.println("calMonthleft :" + leftcalMonth);
			System.out.println("calMonthright :" + rightcalMonth);
			if (monthList.indexOf(leftcalMonth) + 1 == expMonth && (expYear == Integer.parseInt(leftcalYear))) {
				System.out.println("left calander");
				selectLeftCalanderDate(expDate);
				dateNotFound = false;
			} else if (monthList.indexOf(rightcalMonth) + 1 == expMonth
					&& (expYear == Integer.parseInt(rightcalYear))) {
				System.out.println("right calander");
				selectRightCalanderDate(expDate);
				dateNotFound = false;
			} else if (monthList.indexOf(leftcalMonth) + 1 < expMonth && (expYear == Integer.parseInt(leftcalYear))
					|| expYear > Integer.parseInt(leftcalYear)) {
				System.out.println("right arrow");
				locatorService.waitForWebElement("next_arrow").click();
			} else if (monthList.indexOf(leftcalMonth) + 1 > expMonth && (expYear == Integer.parseInt(leftcalYear))
					|| expYear < Integer.parseInt(leftcalYear)) {
				System.out.println("left arrow");
				locatorService.waitForWebElement("pre_arrow").click();
			}
		}
		Thread.sleep(3000);

	}

	public void selectLeftCalanderDate(String date) {
		datePicker = locatorService.waitForWebElement("left_calendar_dates");
		noOfColumns = datePicker.findElements(By.tagName("tr"));
		outer: for (WebElement row : noOfColumns) {
			datePicker1 = row.findElements(By.tagName("td"));
			for (WebElement cell : datePicker1) {
				if (cell.getText().equals(date)) {
					cell.click();
					break outer;
				}
			}
		}

	}

	public void selectRightCalanderDate(String date) {
		datePicker = locatorService.waitForWebElement("right_calendar_dates");
		noOfColumns = datePicker.findElements(By.tagName("tr"));
		outer: for (WebElement row : noOfColumns) {
			datePicker1 = row.findElements(By.tagName("td"));
			for (WebElement cell : datePicker1) {
				if (cell.getText().equals(date)) {
					cell.click();
					break outer;
				}
			}
		}

	}

	public static void main(String[] args) throws InterruptedException {
		PickDate p = new PickDate();
		// p.start();
		p.pickExpDate("2017-08-11");
		p.pickExpDate("2018-02-11");
	}
}
