package com.boomerang.canvas.automation.api;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class FileUtils {

    private static final Gson GSON = new GsonBuilder().create();

    /**
     * Loads the configuration from the config file name that it loads from the classpath.
     * @param configFileName The config file name
     * @param typeClass The type of the class.
     * @param <T> The type information.
     * @return The loaded config into the object.
     * @throws IOException If any error occurs while loading the file.
     */
    public static <T> T loadConfig (String configFileName, Class<T> typeClass) throws IOException {
        String configContent = "{}";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        if (loader == null) {
            loader = FileUtils.class.getClassLoader();
        }
        URL URL = loader.getResource(configFileName);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(loader.getResourceAsStream(configFileName)))) {
            return GSON.fromJson(reader, typeClass);
        }
    }



}
