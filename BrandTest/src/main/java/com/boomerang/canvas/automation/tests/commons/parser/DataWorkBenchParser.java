package com.boomerang.canvas.automation.tests.commons.parser;

import com.boomerang.canvas.automation.api.LocatorProps;
import com.boomerang.canvas.automation.api.LocatorService;
import com.boomerang.canvas.automation.api.ParsingService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@LocatorProps(locatorFileName = "locators/dataworkbench.properties")
public class DataWorkBenchParser implements ParsingService {
	@Inject
	private LocatorService locatorService;

	public String getNoDataText() {
		boolean b = locatorService.isElementPresent("no_Data");
		if (b) {
			return "Data Not Present";
		} else {
			return "Data is Present";
		}
	}
}
