package com.boomerang.canvas.automation.framework;


import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class PropertiesLoader {


    /**
     * Loads the properties configuration from the file.
     * @param propertiesFilePath The properties file path (can be picked up from classpath or absolute path)
     * @return The properties configuration.
     * @throws IOException If any error occurs while finding the file.
     */
    public static PropertiesConfiguration loadProperties (String propertiesFilePath) throws IOException {
        File file = null;
        //ClassLoader loader = Thread.currentThread().getContextClassLoader();
        ClassLoader loader = null;
        loader = loader == null ? PropertiesLoader.class.getClassLoader() : loader;
        try {
            URL fileURL = loader.getResource(propertiesFilePath);
            file = fileURL == null ? null : new File(fileURL.toURI());
        }catch(URISyntaxException e) {
            //ignore.
        }

        file = file == null ? new File(propertiesFilePath) : file;
        Parameters params = new Parameters();
        FileBasedConfigurationBuilder <PropertiesConfiguration> builder = new FileBasedConfigurationBuilder<>
                (PropertiesConfiguration.class).configure(params.properties().setFile(file));
        try {
            return builder.getConfiguration();
        }
        catch(ConfigurationException e)
        {
            throw new IOException(e.getMessage() ,e);
        }
    }


    public static void main(String[] args) throws Exception {
        PropertiesLoader loader = new PropertiesLoader();
        //Properties props = loader.loadProperties("/src/com/boomerang/canvas/bigmovers/bigmoverswidgetlocators.properties");
        //Properties props = loader.loadProperties("/Users/prasun/workspace/athena_qa-automation/Canvas-UI-Automation/src/com/boomerang/canvas/bigmovers/bigmoverswidgetlocators.properties");
        PropertiesConfiguration props = loader.loadProperties("com/boomerang/canvas/bigmovers/bigmoverswidgetlocators.properties");
        System.out.println("props = " + props);
    }
}
