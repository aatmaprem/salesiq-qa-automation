package com.boomerang.canvas.automation.framework;

import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Listeners;

import com.boomerang.canvas.automation.api.RunConfig;
import com.boomerang.data.util.CustomReportUtils;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.relevantcodes.extentreports.ExtentReports;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Listeners(CustomReportUtils.class)
@org.testng.annotations.Guice
public class TestSeriesRunner {

	@Inject
	private RunConfig runConfig;

	private static ExtentReports report;
	private static String series;

	public static String TIMESTAMP = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

	public TestSeriesRunner() {
		Injector injector = Guice.createInjector(GuiceModuleFactory.guiceModule());
		injector.injectMembers(this);
	}

	private static final Logger LOG = LoggerFactory.getLogger(TestSeriesRunner.class);
	public static final String CLASS_NAME_PACKAGE = "com.boomerang.canvas.automation.tests";;

	private void executeSeries(String seriesName, TestSuiteList testSuiteList) throws Exception {

		for (TestSuiteInfo testSuiteInfo : testSuiteList.getSuiteInfoList()) {
			TestSuiteRunner runner = new TestSuiteRunner();
			Map<String, String> list = testSuiteInfo.getParameters();
			runner.executeTestSuite(seriesName, testSuiteInfo);
		}
	}

	/**
	 * Collects the series file entries into the sequence list.
	 * 
	 * @param seriesFilePath
	 *            The path to the series file.
	 * @param seriesSequenceList
	 *            The sequence list in which the sequence of series execution is
	 *            collected.
	 */
	private void parseSeries(String seriesFilePath, List<TestSeriesPart> seriesSequenceList) throws Exception {
		URL seriesFileURL = TestSeriesRunner.class.getResource(seriesFilePath);
		URI seriesFileURI = seriesFileURL == null ? null : seriesFileURL.toURI();
		Path seriesFileAbsolutePath = seriesFileURI != null ? Paths.get(seriesFileURI) : Paths.get(seriesFilePath);

		List<String> seriesLines = Files.readAllLines(seriesFileAbsolutePath);
		List<TestSuiteInfo> suiteList = new ArrayList<>();
		for (String seriesLine : seriesLines) {
			seriesLine = seriesLine.trim();
			if (seriesLine.startsWith("#") || seriesLine.isEmpty()) {
				continue;
			}
			if (seriesLine.endsWith(".series")) {
				if (!suiteList.isEmpty()) {
					TestSeriesPart seriesPart = new TestSeriesPart(seriesFilePath, new TestSuiteList(suiteList));
					seriesSequenceList.add(seriesPart);
					suiteList.clear();
				}
				parseSeries(resolve(seriesFileAbsolutePath.toString(), seriesLine), seriesSequenceList);

				continue;
			}

			suiteList.add(parseSeriesLine(seriesLine));

		}

		if (!suiteList.isEmpty()) {

			TestSeriesPart seriesPart = new TestSeriesPart(seriesFilePath, new TestSuiteList(suiteList));

			seriesSequenceList.add(seriesPart);
			suiteList.clear();
		}
	}

	/**
	 * Executes the series recursively until it has run all the test suites.
	 * 
	 * @param seriesFileName
	 *            The name of the series file name.
	 * @throws Exception
	 *             If any error occurs while running the series file.
	 */
	public void executeSeries(String seriesFileName) throws Exception {
		List<TestSeriesPart> seriesPartList = new ArrayList<>();
		String workspace = runConfig.getUserDir();
		series = capitalizeFirstLetter(System.getProperty("Suitename"));
		setSeries(series);

		report = new ExtentReports(workspace + "/" + series + "-Suite" + TIMESTAMP + "-" + runConfig.getEnv() + "-"
				+ runConfig.getClientName() + "-" + runConfig.getBrowser() + "/ExtendedReports.html");
		report.loadConfig(new File(workspace + "/extent-config.xml"));
		report.addSystemInfo("Environment", runConfig.getEnv() + "-" + runConfig.getClientName());
		report.addSystemInfo("User Name", System.getProperty("user.name"));
		report.addSystemInfo("Browser", capitalizeFirstLetter(runConfig.getBrowser()));
		setReport(report);
		parseSeries(seriesFileName, seriesPartList);
		for (TestSeriesPart seriesPart : seriesPartList) {
			executeSeries(seriesPart.getSeriesName(), seriesPart.getSuiteList());
		}
	}

	/**
	 * set and get the series name for report folder
	 * 
	 * @return
	 */
	public String setSeries(String series) {
		return this.series = series;
	}

	public String getSeries() {
		return this.series;
	}

	public ExtentReports setReport(ExtentReports reportFile) {
		return this.report = reportFile;
	}

	public ExtentReports getReport() {
		return this.report;
	}

	/**
	 * This method will give string with camel case
	 * 
	 * @param value
	 *            of string
	 * @return camel case string
	 */
	private static String capitalizeFirstLetter(String str) {
		return WordUtils.capitalizeFully(str);
	}

	private String resolve(String parentSeriesFile, String childSeriesFile) {
		Path parentSeriesPath = Paths.get(parentSeriesFile);
		Path childSeriesPath = Paths.get(childSeriesFile);

		Path path = parentSeriesPath.getParent().resolve(childSeriesPath);
		return path.toString();

	}

	/**
	 * Parses the series line into a test suite info object.
	 * 
	 * @param line
	 *            The series line.
	 * @return The test suite info.
	 * @throws ClassNotFoundException
	 *             If any error occurs while loading the class.
	 */

	private TestSuiteInfo parseSeriesLine(String line) throws ClassNotFoundException {

		line = line.trim();
		String[] classNameSplit = line.split(" ");
		String className = classNameSplit[0];

		className = fullClassName(className);

		String[] paramsSplit = line.split("-");

		Map<String, String> paramsMap = new HashMap<>();
		for (String paramPair : paramsSplit) {
			String[] paramKeyValue = paramPair.split("##");
			String key = paramKeyValue[0];
			String value = paramKeyValue.length > 1 ? paramKeyValue[1] : "true";
			paramsMap.put(key, value);
		}

		return new TestSuiteInfo(TestSeriesRunner.class.getClassLoader().loadClass(className), paramsMap);

	}

	private String fullClassName(String className) {
		if (className.startsWith(CLASS_NAME_PACKAGE)) {
			return className;
		}

		return CLASS_NAME_PACKAGE + "." + className;
	}

	private static class TestSeriesPart {

		private String seriesName;
		private TestSuiteList suiteList;

		public TestSeriesPart(String seriesName, TestSuiteList suiteList) {
			this.seriesName = seriesName;
			this.suiteList = suiteList;
		}

		public String getSeriesName() {
			return seriesName;
		}

		public TestSuiteList getSuiteList() {
			return suiteList;
		}
	}

	public static void main(String[] args) throws Exception {
		TestSeriesRunner runner = new TestSeriesRunner();
		String browser = null;
		String suiteName = null;
		if (System.getProperty("Suitename").equals("basicsanity")) {
			suiteName = "/" + System.getProperty("Suitename") + ".series";
		} else if (System.getProperty("Suitename").contains("canvasbasicsanity")) {
			suiteName = "/" + System.getProperty("Suitename") + ".series";
		}
		browser = System.getProperty("Browser") == null ? "chrome" : System.getProperty("Browser");

		String[] browserType = browser.split(",");
		for (int i = 0; i < browserType.length; i++) {
			System.setProperty("BrowserType", browserType[i]);
			runner.executeSeries(suiteName);
			ScreenshotListener sl = new ScreenshotListener();
			if (System.getProperty("Suitename").equals("basicsanity")) {
				sl.uploadReport(System.getProperty("user.dir"));
			} else {
				sl.uploadCanvasReport(System.getProperty("user.dir"));
			}
		}

	}
}
