package com.boomerang.canvas.automation.tests.basicsanity;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Guice;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.boomerang.canvas.automation.api.LoginAction;
import com.boomerang.canvas.automation.api.NavigatorService;
import com.boomerang.canvas.automation.api.RunConfig;
import com.boomerang.canvas.automation.api.TestSuite;
import com.boomerang.canvas.automation.framework.GuiceModuleFactory;
import com.boomerang.canvas.automation.framework.TestListener;
import com.boomerang.canvas.automation.tests.commons.actions.PreCannedDateAction;
import com.boomerang.canvas.automation.tests.commons.parser.InsightsPageParser;
import com.boomerang.data.config.ConfigLoader;
import com.boomerang.data.database.executer.QueryExecuter;
import com.boomerang.data.util.AssertUtils;
import com.boomerang.data.util.CustomReportParameters;
import com.boomerang.data.util.GenericUtil;
import com.google.inject.Inject;

import com.boomerang.data.util.CustomReportUtils;

@Listeners(CustomReportUtils.class)
@TestSuite(name = "BasicSanityTest", description = "This suite has test cases for widgets presenent")
@Guice(moduleFactory = GuiceModuleFactory.class)
public class BasicSanityTest {
	private static final Logger LOG = LoggerFactory.getLogger(TestListener.class);

	@Inject
	private RunConfig runConfig;

	@Inject
	private NavigatorService navigatorService;
	@Inject
	SoftAssert softAssert;
	@Inject
	private LoginAction loginAction;
	@Inject
	private QueryExecuter queryExecuter;
	@Inject
	GenericUtil generic;
	@Inject
	private PreCannedDateAction preCannedDateAction;
	@Inject
	private InsightsPageParser insightsPageParser;
	@Inject
	private AssertUtils assertUtils;

	private String clientName;

	@Parameters({ "clientpath" })
	@BeforeClass
	public void beforeClass(String clientpath) throws Exception {
		this.clientName = clientpath;
		queryExecuter = new QueryExecuter(clientName);
		LOG.info(" configuration completed for " + clientName);
		Reporter.getCurrentTestResult().setAttribute("client", clientName);
		LOG.info("\n Client: " + clientName);
		try {
			login();
		} catch (Exception e) {
			LOG.info("Retry Method");

			login();
		}
	}

	public void login() throws Exception {
		navigatorService.openWebURL(runConfig.getClientURL(), LoginAction.USERNAME_TEXTBOX);
		loginAction.login(runConfig.getUserName(), runConfig.getPassword());
		loginAction.pageNavigation();
		preCannedDateAction.clickOnCalendarIcon();
		preCannedDateAction.dateSelection(ConfigLoader.getMinTransactionDate());
		preCannedDateAction.dateSelection(ConfigLoader.getMaxTransactionDate());

	}

	@Test(enabled = true, testName = "RetailerRevenue", description = "Validate the RetailerRevenue value from canvas to Source table")
	public void retailerRevenue() {

		String finalRevenue = "104600931";// insightsPageParser.getRetailerRevenue();
		assertUtils.assertInitialization();

		ResultSet sorrceResultSet = queryExecuter.getSourceData();
		String sourceQuery = queryExecuter.getSourceQuery();

		Map<String, Long> sourceRevenueSet = queryExecuter.getRevenue(sorrceResultSet);
		long sourceRevenue = sourceRevenueSet.get("RETAILER REVENUE");
		double revenueMismatchedpercentage = queryExecuter.getMismatchedPercentage(sourceRevenue,
				Math.abs(sourceRevenue - Long.parseLong(finalRevenue)));

		insertDataIntoReport("retailerRevenue", "client_sales_widget_data", "RetailerRevenue", sourceRevenue,
				sourceQuery, Long.parseLong(finalRevenue), revenueMismatchedpercentage);
		assertUtils.dataValidation(revenueMismatchedpercentage, sourceRevenue, Long.parseLong(finalRevenue));
		assertUtils.assertAll();
	}

	public void insertDataIntoReport(String testCase, String cubename, String testName, Object sourceValue,
			Object sourceQuery, Object finalValue, Object mismatch) {
		List<CustomReportParameters> reportResultVaribles = new ArrayList<>();
		Map<String, List<CustomReportParameters>> report = new HashMap<String, List<CustomReportParameters>>();

		reportResultVaribles.add(new CustomReportParameters("client_sales_widget_data", testName, sourceValue,
				sourceQuery, finalValue, testName, mismatch));
		report.put(testCase, reportResultVaribles);
		Reporter.getCurrentTestResult().setAttribute("extra_data", report);
	}
}
