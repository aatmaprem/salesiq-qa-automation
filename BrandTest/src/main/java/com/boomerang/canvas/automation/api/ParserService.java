package com.boomerang.canvas.automation.api;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;

@Singleton
public class ParserService {

	@Inject
	private WebDriver webDriver;

	private Document jsoupParser() {
		String javascript = "return arguments[0].innerHTML";
		String pageSource = (String) ((JavascriptExecutor) webDriver).executeScript(javascript,
				webDriver.findElement(By.tagName("html")));
		return Jsoup.parse(pageSource);
	}

	public String parseContent(String cssSelector) {
		return jsoupParser().select(cssSelector).text();
	}

	public List<String> getListOfElements(String cssSelector) {
		List<String> s = new ArrayList<String>();
		Elements elements = jsoupParser().select(cssSelector);

		for (int index = 0; index < elements.size(); index++)

			s.add(elements.get(index).text());
		return s;
	}

	public int getElementsCount(String cssSelector, int num) {
		int s = 0;
		Elements elements = jsoupParser().select(cssSelector);
		s = elements.get(num).select(".darkIndicator").size();
		return s;

	}

	public Elements parseElements(String cssSelector) {
		return jsoupParser().select(cssSelector);
	}
}
